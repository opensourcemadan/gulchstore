<?php
/**
 * Contact section.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */
?>
		<section class="module">
			<div class="container">

				<div class="row">
				
					<?php

						$tradiestandard_contact_page_form_shortcode = get_theme_mod( 'tradiestandard_contact_page_form_shortcode' );

						$is_content = $post->post_content !== '' ? true : false;
						$is_shotcode = ! empty( $tradiestandard_contact_page_form_shortcode ) ? true : false;

					if ( $is_shotcode ) {

						echo '<div class="col-xs-12  ' . ( $is_content ? 'col-sm-6' : 'col-sm-12' ) . ' contact-page-form">';

						echo do_shortcode( $tradiestandard_contact_page_form_shortcode );

						echo '</div>';

					}

					if ( $is_content ) {

						echo '<div class="col-xs-12 ' . ( $is_shotcode ? 'col-sm-6' : 'col-sm-12' ) . '">';

						the_content();

						echo '</div>';
					}

					?>

				</div><!-- .row -->

			</div>
		</section>
		<!-- Contact end -->

		<!-- Map start -->
		<?php
			$tradiestandard_contact_page_map_shortcode = get_theme_mod( 'tradiestandard_contact_page_map_shortcode' );
		if ( ! empty( $tradiestandard_contact_page_map_shortcode ) ) :
			echo '<section id="map-section">';
			echo '<div id="map">' . do_shortcode( $tradiestandard_contact_page_map_shortcode ) . '</div>';
			echo '</section>';
			endif;
		?>
		<!-- Map end -->
