<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage tradiestandard
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php

	/*
	 * @hooked tradiestandard_page_content - 20
	 */
	do_action( 'tradiestandard_page' );
	?>
</article><!-- #post-## -->
