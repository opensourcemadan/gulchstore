<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage tradiestandard
 */


	/*
	 * @hooked tradiestandard_post_header - 10
	 * @hooked tradiestandard_post_meta - 20
	 * @hooked tradiestandard_post_content - 30
	 */
	do_action( 'tradiestandard_single_post' );


