<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage tradiestandard
 */
?>

<div <?php post_class( 'post' ); ?> id="post-<?php the_ID(); ?>" itemscope="" itemtype="http://schema.org/BlogPosting">

	<?php

	/*
	 * @hooked tradiestandard_post_header() - 10
	 * @hooked tradiestandard_post_meta() - 20
	 * @hooked tradiestandard_post_content() - 30
	 */
	do_action( 'tradiestandard_loop_post' );
	?>

</div><!-- #post-## -->
