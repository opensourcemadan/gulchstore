<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage tradiestandard
 */
?>
<?php do_action( 'tradiestandard_before_footer' ); ?>

	<?php do_action( 'tradiestandard_footer' ); ?>
	
	</div>
	<!-- Wrapper end -->
	
	<!-- Scroll-up -->
	<div class="scroll-up">
		<a href="#totop"><i class="arrow_carrot-2up"></i></a>
	</div>

	<?php do_action( 'tradiestandard_after_footer' ); ?>

<?php wp_footer(); ?>

</body>
</html>
