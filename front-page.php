<?php
/**
 * The front-page.php
 *
 * @package tradiestandard
 */
get_header();
/* Wrapper start */

echo '<div class="main">';
$big_title = get_template_directory() . '/inc/sections/tradiestandard_big_title_section.php';
load_template( apply_filters( 'tradiestandard-subheader', $big_title ) );

/******  Slider Section *******/
$slider_section = get_template_directory() . '/inc/sections/tradiestandard_slider_section.php';
require_once( $slider_section );

/* Wrapper start */
$tradiestandard_bg = get_theme_mod( 'background_color' );

if ( isset( $tradiestandard_bg ) && $tradiestandard_bg != '' ) {
	echo '<div class="main front-page-main" style="background-color: #' . $tradiestandard_bg . '">';
} else {

	echo '<div class="main front-page-main" style="background-color: #FFF">';

}

// text content below slider
if( have_posts() ) :
echo '<div class="home-text-content"><div class="container"><div class="row">';
while( have_posts() ) : the_post();
the_title('<h3 class="module-title section-title col-sm-12" style="text-align: center;">', '</h3>');
echo '<div class="section-content col-sm-12" style="text-align: center;">';
	the_content();
echo '</div>';
endwhile;
echo '</div></div></div>';
endif;



/******* Products Section *********/
$latest_products = get_template_directory() . '/inc/sections/tradiestandard_products_section.php';
require_once( $latest_products );

/******* Video Section *********/
$video = get_template_directory() . '/inc/sections/tradiestandard_video_section.php';
require_once( $video );

/******  Banners Section *******/
$banners_section = get_template_directory() . '/inc/sections/tradiestandard_banners_section.php';
require_once( $banners_section );

/******* Products Slider Section *********/
$products_slider = get_template_directory() . '/inc/sections/tradiestandard_products_slider_section.php';
require_once( $products_slider );

// highight text above footer
echo '<div class="home-contact-banner"><div class="container"><div class="row">';
echo '<div class="col-sm-8 col-md-9">';
echo '<h3 class="module-title section-title col-sm-12" style="text-align: left;">You want to Join Us!
</h3>';
echo '<div class="section-content col-sm-12" style="text-align: left;"> If you would like to Join Us, contact us for more information!
 </div>';
 echo '</div>';
 echo '<div class="col-sm-4 col-md-3">';
 	echo '<a href="/contact" class="btn btn-outline-warning">Contact Us</a>';
 echo "</div>";
echo '</div></div></div>';

get_footer();