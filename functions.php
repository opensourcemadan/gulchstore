<?php
/**
 * Main functions file
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

/**
 * Initialize all the things.
 */
require get_template_directory() . '/inc/init.php';

/**
 * product category custom column
 * product banner
*/
// add column called 'banner'
function product_taxonomy_columns( $columns )
{
	$columns['product_term_banner'] = __('Banner');

	return $columns;
}
add_filter('manage_edit-product_cat_columns' , 'product_taxonomy_columns');

// return uploaded banner image to column
function product_taxonomy_columns_content( $content, $column_name, $term_id )
{
    if ( 'product_term_banner' == $column_name ) {
    	$image = wp_get_attachment_image( get_term_meta( $term_id, 'rask_term_banner_id', 1 ), 'medium' );
        $content = $image;
    }
	return $content;
}
add_filter( 'manage_product_cat_custom_column', 'product_taxonomy_columns_content', 10, 3 );
