<?php
/**
 * The header for our theme.
 *
 * @package tradiestandard
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> <?php tradiestandard_html_tag_schema(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<?php do_action( 'tradiestandard_before_header' ); ?>

	<!-- Preloader -->
	<?php

	/* Preloader */
	if ( is_front_page() && ! is_customize_preview() && get_option( 'show_on_front' ) != 'page' ) :

		$tradiestandard_disable_preloader = get_theme_mod( 'tradiestandard_disable_preloader' );

		if ( isset( $tradiestandard_disable_preloader ) && ($tradiestandard_disable_preloader != 1) ) :

			echo '<div class="page-loader">';
				echo '<div class="loader">' . __( 'Loading...','tradiestandard' ) . '</div>';
			echo '</div>';

		endif;

	endif;



	?>
	
	<?php do_action( 'tradiestandard_header' ); ?>

	<?php do_action( 'tradiestandard_after_header' ); ?>
