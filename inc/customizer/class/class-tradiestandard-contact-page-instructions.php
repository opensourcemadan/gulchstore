<?php
/**
 * Contact page template instructions.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */


/**
 * Class tradiestandard_Contact_Page_Instructions
 */
class tradiestandard_Contact_Page_Instructions extends WP_Customize_Control {

	/**
	 * Render Content Function
	 */
	public function render_content() {
		echo __( 'To customize the Contact Page you need to first select the template "Contact page" for the page you want to use for this purpose. Then open that page in the browser and press "Customize" in the top bar.', 'tradiestandard' ) . '<br><br>';
	}
}
