<?php
/**
 * Customizer functionality for the Advanced Options section.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

/**
 * Hook controls for Advanced Options section to Customizer.
 */
function tradiestandard_advanced_customize_register( $wp_customize ) {

	/*  ADVANCED OPTIONS  */

	$wp_customize->add_section( 'tradiestandard_general_section', array(
		'title'    => __( 'Advanced options', 'tradiestandard' ),
		'priority' => 55,
	) );

	/* Disable preloader */
	$wp_customize->add_setting( 'tradiestandard_disable_preloader', array(
		'sanitize_callback' => 'tradiestandard_sanitize_text',
		'transport'         => 'postMessage',
	) );

	$wp_customize->add_control( 'tradiestandard_disable_preloader', array(
		'type'        => 'checkbox',
		'label'       => __( 'Disable preloader?', 'tradiestandard' ),
		'section'     => 'tradiestandard_general_section',
		'priority'    => 1,
	) );

	/* Body font size */
	$wp_customize->add_setting( 'tradiestandard_font_size', array(
		'sanitize_callback' => 'tradiestandard_sanitize_text',
		'default' => '13px',
	) );

	$wp_customize->add_control(
		'tradiestandard_font_size',
		array(
			'type'     => 'select',
			'label'    => 'Select font size:',
			'section'  => 'tradiestandard_general_section',
			'choices'  => array(
				'12px' => '12px',
				'13px' => '13px',
				'14px' => '14px',
				'15px' => '15px',
				'16px' => '16px',
			),
			'priority' => 2,
		)
	);
}

add_action( 'customize_register', 'tradiestandard_advanced_customize_register' );
