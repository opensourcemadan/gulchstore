<?php
/**
 * Customizer functionality for the Blog Header.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

/**
 * Hook controls for Blog Header.
 */
function tradiestandard_blog_header_customize_register( $wp_customize ) {

	/* Blog Header title */
	$wp_customize->add_setting( 'tradiestandard_blog_header_title', array(
		'default' 			=> __( 'Blog','tradiestandard' ),
		'sanitize_callback' => 'tradiestandard_sanitize_text',
		'transport' 		=> 'postMessage',
	));
	$wp_customize->add_control( 'tradiestandard_blog_header_title', array(
		'label'    			=> esc_html__( 'Blog header title', 'tradiestandard' ),
		'section'  			=> 'tradiestandard_header_section',
		'priority'    		=> 3,
	));

	/* Blog Header subtitle */
	$wp_customize->add_setting( 'tradiestandard_blog_header_subtitle', array(
		'sanitize_callback' => 'tradiestandard_sanitize_text',
		'transport' 		=> 'postMessage',
	));
	$wp_customize->add_control( 'tradiestandard_blog_header_subtitle', array(
		'label'    			=> esc_html__( 'Blog header subtitle', 'tradiestandard' ),
		'section'  			=> 'tradiestandard_header_section',
		'priority'    		=> 4,
	));

}

add_action( 'customize_register', 'tradiestandard_blog_header_customize_register' );
