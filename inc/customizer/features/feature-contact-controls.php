<?php
/**
 * Customizer functionality for the Contact Page controls.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

/**
 * Hook controls for the Contact Page to Customizer.
 */
function tradiestandard_contact_page_customize_register( $wp_customize ) {

	require_once( trailingslashit( get_template_directory() ) . 'inc/customizer/class/class-tradiestandard-contact-page-instructions.php' );

	/*  Contact page  */
	$wp_customize->add_section( 'tradiestandard_contact_page_section', array(
		'title'    => __( 'Contact page', 'tradiestandard' ),
		'priority' => 99,
	) );

	/* Contact Form  */
	$wp_customize->add_setting( 'tradiestandard_contact_page_form_shortcode', array(
		'sanitize_callback' => 'tradiestandard_sanitize_text',
	) );

	$wp_customize->add_control( 'tradiestandard_contact_page_form_shortcode', array(
		'label'           => __( 'Contact form shortcode', 'tradiestandard' ),
		'description'     => sprintf(
			/* translators: 1: Link to Pirate Forms Plugin. */
			__( 'Create a form, copy the shortcode generated and paste it here. We recommend %1$s but you can use any plugin you like.', 'tradiestandard' ),
			sprintf(
				/* translators: 1: 'Simple Contact Form Plugin - PirateForms' */
				'<a href="https://wordpress.org/plugins/pirate-forms/" target="_blank">%s</a>',
			'Simple Contact Form Plugin - PirateForms' )
		),
		'section'         => 'tradiestandard_contact_page_section',
		'active_callback' => 'tradiestandard_is_contact_page',
		'priority'        => 1,
	) );

	/* Map ShortCode  */
	$wp_customize->add_setting( 'tradiestandard_contact_page_map_shortcode', array(
		'sanitize_callback' => 'tradiestandard_sanitize_text',
	) );

	$wp_customize->add_control( 'tradiestandard_contact_page_map_shortcode', array(
		'label'           => __( 'Map shortcode', 'tradiestandard' ),
		'description'     => __( 'To use this section please install <a href="https://wordpress.org/plugins/intergeo-maps/">Intergeo Maps</a> plugin then use it to create a map and paste here the shortcode generated', 'tradiestandard' ),
		'section'         => 'tradiestandard_contact_page_section',
		'active_callback' => 'tradiestandard_is_contact_page',
		'priority'        => 2,
	) );

	/*  Contact page - instructions for users when not on Contact page  */

	$wp_customize->add_section( 'tradiestandard_contact_page_instructions', array(
		'title'    => __( 'Contact page', 'tradiestandard' ),
		'priority' => 99,
	) );

	$wp_customize->add_setting( 'tradiestandard_contact_page_instructions', array(
		'sanitize_callback' => 'tradiestandard_sanitize_text',
	) );

	$wp_customize->add_control( new tradiestandard_Contact_Page_Instructions( $wp_customize, 'tradiestandard_contact_page_instructions', array(
		'section'         => 'tradiestandard_contact_page_instructions',
		'active_callback' => 'tradiestandard_is_not_contact_page',
	) ) );
}

add_action( 'customize_register', 'tradiestandard_contact_page_customize_register' );

/**
 * Check if is contact page.
 *
 * @return bool
 */
function tradiestandard_is_contact_page() {
	return is_page_template( 'template-contact.php' );
};

/**
 * Check if is not contact page.
 *
 * @return bool
 */
function tradiestandard_is_not_contact_page() {
	return ! is_page_template( 'template-contact.php' );
};
