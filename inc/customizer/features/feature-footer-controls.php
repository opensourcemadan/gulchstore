<?php
/**
 * Customizer functionality for the Footer.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

/**
 * Hook controls for Footer to Customizer.
 */
function tradiestandard_footer_customize_register( $wp_customize ) {

	/*  Footer */

	$wp_customize->add_section( 'tradiestandard_footer_section', array(
		'title'    => __( 'Footer', 'tradiestandard' ),
		'priority' => 50,
	) );

	/* Copyright */
	$wp_customize->add_setting( 'tradiestandard_copyright', array(
		'sanitize_callback' => 'tradiestandard_sanitize_text',
		'transport'         => 'postMessage',
	) );

	$wp_customize->add_control( 'tradiestandard_copyright', array(
		'label'    => __( 'Copyright', 'tradiestandard' ),
		'section'  => 'tradiestandard_footer_section',
		'priority' => 1,
	) );

	/* Hide site info */
	$wp_customize->add_setting( 'tradiestandard_site_info_hide', array(
		'transport' => 'postMessage',
		'sanitize_callback' => 'tradiestandard_sanitize_text',
	));

	$wp_customize->add_control(
		'tradiestandard_site_info_hide',
		array(
			'type' => 'checkbox',
			'label' => __( 'Hide site info?','tradiestandard' ),
			'section' => 'tradiestandard_footer_section',
			'priority' => 2,
		)
	);

	/* socials */
	$wp_customize->add_setting( 'tradiestandard_socials', array(
		'transport'         => 'postMessage',
		'sanitize_callback' => 'tradiestandard_sanitize_repeater',
	) );

	$wp_customize->add_control( new tradiestandard_Repeater_Controler( $wp_customize, 'tradiestandard_socials', array(
		'label'                         => __( 'Add new social', 'tradiestandard' ),
		'section'                       => 'tradiestandard_footer_section',
		'active_callback'               => 'is_front_page',
		'priority'                      => 3,
		'tradiestandard_image_control'       => false,
		'tradiestandard_link_control'        => true,
		'tradiestandard_text_control'        => false,
		'tradiestandard_subtext_control'     => false,
		'tradiestandard_label_control'       => false,
		'tradiestandard_icon_control'        => true,
		'tradiestandard_description_control' => false,
		'tradiestandard_box_label'           => __( 'Social', 'tradiestandard' ),
		'tradiestandard_box_add_label'       => __( 'Add new social', 'tradiestandard' ),
	) ) );
}

add_action( 'customize_register', 'tradiestandard_footer_customize_register' );
