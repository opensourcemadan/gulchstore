<?php
/**
 * Customizer functionality for the Banners Section.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

/**
 * Hook controls for Banners Section to Customizer.
 */
function tradiestandard_banners_controls_customize_register( $wp_customize ) {

	/* Banners section */

	$wp_customize->add_section( 'tradiestandard_banners_section', array(
		'title'    => __( 'Banners section', 'tradiestandard' ),
		'priority' => apply_filters( 'tradiestandard_section_priority', 15, 'tradiestandard_banners_section' ),
	) );

	/* Hide banner */
	$wp_customize->add_setting( 'tradiestandard_banners_hide', array(
		'transport'         => 'postMessage',
		'sanitize_callback' => 'tradiestandard_sanitize_text',
	) );

	$wp_customize->add_control(
		'tradiestandard_banners_hide',
		array(
			'type'        => 'checkbox',
			'label'       => __( 'Hide banners section?', 'tradiestandard' ),
			'section'     => 'tradiestandard_banners_section',
			'priority'    => 1,
		)
	);

	$wp_customize->add_setting( 'tradiestandard_banners_title', array(
		'transport'         => 'postMessage',
		'sanitize_callback' => 'tradiestandard_sanitize_text',
	) );

	$wp_customize->add_control( 'tradiestandard_banners_title', array(
		'label'    => __( 'Section title', 'tradiestandard' ),
		'section'  => 'tradiestandard_banners_section',
		'priority' => 2,
	) );

	/* Banner */
	$wp_customize->add_setting( 'tradiestandard_banners', array(
		'transport'         => 'postMessage',
		'sanitize_callback' => 'tradiestandard_sanitize_repeater',
		'default'           => json_encode( array(
			array(
				'image_url' => get_template_directory_uri() . '/assets/images/banner1.jpg',
				'link'      => '#',
			),
			array(
				'image_url' => get_template_directory_uri() . '/assets/images/banner2.jpg',
				'link' => '#',
			),
			array(
				'image_url' => get_template_directory_uri() . '/assets/images/banner3.jpg',
				'link' => '#',
			),
		) ),
	) );
	$wp_customize->add_control( new tradiestandard_Repeater_Controler( $wp_customize, 'tradiestandard_banners', array(
		'label'                         => __( 'Add new banner', 'tradiestandard' ),
		'section'                       => 'tradiestandard_banners_section',
		'active_callback'               => 'is_front_page',
		'priority'                      => 3,
		'tradiestandard_image_control'       => true,
		'tradiestandard_link_control'        => true,
		'tradiestandard_text_control'        => false,
		'tradiestandard_subtext_control'     => false,
		'tradiestandard_label_control'       => false,
		'tradiestandard_icon_control'        => false,
		'tradiestandard_description_control' => false,
		'tradiestandard_box_label'           => __( 'Banner', 'tradiestandard' ),
		'tradiestandard_box_add_label'       => __( 'Add new banner', 'tradiestandard' ),
	) ) );

	$wp_customize->get_section( 'tradiestandard_banners_section' )->panel = 'tradiestandard_front_page_sections';

}

add_action( 'customize_register', 'tradiestandard_banners_controls_customize_register' );
