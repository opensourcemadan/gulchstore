<?php
/**
 * Customizer functionality for the Slider Section.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

/**
 * Hook controls for Big Title Section to Customizer.
 */
function tradiestandard_big_title_controls_customize_register( $wp_customize ) {

	/* Big title section */

	$wp_customize->add_section( 'tradiestandard_big_title_section' , array(
		'title'       => __( 'Big title section', 'tradiestandard' ),
		'priority'    => 10,
		'panel' => 'tradiestandard_front_page_sections',
	));

	/* Hide big title section */
	$wp_customize->add_setting( 'tradiestandard_big_title_hide', array(
		'sanitize_callback' => 'tradiestandard_sanitize_text',
		'transport' => 'postMessage',
	));

	$wp_customize->add_control(
		'tradiestandard_big_title_hide',
		array(
			'type' => 'checkbox',
			'label' => __( 'Hide big title section?','tradiestandard' ),
			'section' => 'tradiestandard_big_title_section',
			'priority'    => 1,
		)
	);

	/* Image */
	$wp_customize->add_setting( 'tradiestandard_big_title_image', array(
		'sanitize_callback' => 'esc_url_raw',
		'default' => get_template_directory_uri() . '/assets/images/slide1.jpg',
	));

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'tradiestandard_big_title_image', array(
		'label' => __( 'Image', 'tradiestandard' ),
		'section' => 'tradiestandard_big_title_section',
		'priority' => 2,
	)));

	/* Title */
	$wp_customize->add_setting( 'tradiestandard_big_title_title', array(
		'sanitize_callback' => 'tradiestandard_sanitize_text',
		'default'    => 'Rask',
	));

	$wp_customize->add_control( 'tradiestandard_big_title_title', array(
		'label' => __( 'Title','tradiestandard' ),
		'section'  => 'tradiestandard_big_title_section',
		'priority'    => 3,
	));

	/* Subtitle */
	$wp_customize->add_setting( 'tradiestandard_big_title_subtitle', array(
		'sanitize_callback' => 'tradiestandard_sanitize_text',
		'default'    => __( 'WooCommerce Theme', 'tradiestandard' ),
	));

	$wp_customize->add_control( 'tradiestandard_big_title_subtitle', array(
		'label' => __( 'Subtitle', 'tradiestandard' ),
		'section'  => 'tradiestandard_big_title_section',
		'priority'    => 4,
	));

	/* Button label */
	$wp_customize->add_setting( 'tradiestandard_big_title_button_label', array(
		'sanitize_callback' => 'tradiestandard_sanitize_text',
		'default'    => __( 'Read more', 'tradiestandard' ),
	));

	$wp_customize->add_control( 'tradiestandard_big_title_button_label', array(
		'label' => __( 'Button label','tradiestandard' ),
		'section'  => 'tradiestandard_big_title_section',
		'priority'    => 5,
	));

	/* Button link */
	$wp_customize->add_setting( 'tradiestandard_big_title_button_link', array(
		'sanitize_callback' => 'tradiestandard_sanitize_text',
		'default'    => __( '#', 'tradiestandard' ),
	));

	$wp_customize->add_control( 'tradiestandard_big_title_button_link', array(
		'label' => __( 'Button link', 'tradiestandard' ),
		'section'  => 'tradiestandard_big_title_section',
		'priority'    => 6,
	));

}
add_action( 'customize_register', 'tradiestandard_big_title_controls_customize_register' );
