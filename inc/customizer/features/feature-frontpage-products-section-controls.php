<?php
/**
 * Customizer functionality for the Products Section.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

/**
 * Hook controls for Products Section to Customizer.
 */
function tradiestandard_products_controls_customize_register( $wp_customize ) {

	/* Products section */

	$tradiestandard_require_woo = '';
	if ( ! class_exists( 'WooCommerce' ) ) {
		$tradiestandard_require_woo = '<div class="tradiestandard-require-woo"><p>' . sprintf(
			/* translators: 1: Link to WooCommerce Plugin */
				__( 'To use this section, you are required to first install the  %1$s plugin', 'tradiestandard' ),
			sprintf(
				/* translators: 1: Link to WiooCommerce Plugin. 2: 'WooCommerce' */
				'<a href="' . esc_url( wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=woocommerce' ),
				'install-plugin_woocommerce' ) ) . '">%s</a>', esc_html__( 'WooCommerce', 'tradiestandard' )
			)
		) . '</p></div>';
	}

	$wp_customize->add_section( 'tradiestandard_products_section', array(
		'default'     => false,
		'title'       => __( 'Products section', 'tradiestandard' ),
		'description' => $tradiestandard_require_woo,
		'priority'    => apply_filters( 'tradiestandard_section_priority', 20, 'tradiestandard_products_section' ),
	) );

	/* Hide products */
	$wp_customize->add_setting( 'tradiestandard_products_hide', array(
		'sanitize_callback' => 'tradiestandard_sanitize_text',
	) );

	$wp_customize->add_control( 'tradiestandard_products_hide', array(
		'type'     => 'checkbox',
		'label'    => __( 'Hide products section?', 'tradiestandard' ),
		'section'  => 'tradiestandard_products_section',
		'priority' => 1,
	) );

	/* Title */
	$wp_customize->add_setting( 'tradiestandard_products_title', array(
		'transport'         => 'postMessage',
		'sanitize_callback' => 'tradiestandard_sanitize_text',
		'default'           => __( 'Latest products', 'tradiestandard' ),
	) );

	$wp_customize->add_control( 'tradiestandard_products_title', array(
		'label'    => __( 'Section title', 'tradiestandard' ),
		'section'  => 'tradiestandard_products_section',
		'priority' => 2,
	) );

	/* Shortcode */
	$wp_customize->add_setting( 'tradiestandard_products_shortcode', array(
		'sanitize_callback' => 'tradiestandard_sanitize_text',
	) );

	$wp_customize->add_control( 'tradiestandard_products_shortcode', array(
		'label'       => __( 'WooCommerce shortcode', 'tradiestandard' ),
		'section'     => 'tradiestandard_products_section',
		'description' => __( 'Insert a WooCommerce shortcode', 'tradiestandard' ),
		'priority'    => 3,
	) );

	/* Category */
	$tradiestandard_prod_categories_array = array(
		'-' => __( 'Select category', 'tradiestandard' ),
	);

	$tradiestandard_prod_categories = get_categories( array(
		'taxonomy'   => 'product_cat',
		'hide_empty' => 0,
		'title_li'   => '',
	) );

	if ( ! empty( $tradiestandard_prod_categories ) ) :
		foreach ( $tradiestandard_prod_categories as $tradiestandard_prod_cat ) :

			if ( ! empty( $tradiestandard_prod_cat->term_id ) && ! empty( $tradiestandard_prod_cat->name ) ) :
				$tradiestandard_prod_categories_array[ $tradiestandard_prod_cat->term_id ] = $tradiestandard_prod_cat->name;
			endif;

		endforeach;
	endif;

	$wp_customize->add_setting( 'tradiestandard_products_category', array(
		'sanitize_callback' => 'tradiestandard_sanitize_text',
	) );
	$wp_customize->add_control( 'tradiestandard_products_category', array(
		'type'        => 'select',
		'label'       => __( 'Products category', 'tradiestandard' ),
		'description' => __( 'OR pick a product category. If no shortcode or no category is selected , WooCommerce latest products are displaying.', 'tradiestandard' ),
		'section'     => 'tradiestandard_products_section',
		'choices'     => $tradiestandard_prod_categories_array,
		'priority'    => 4,
	) );

	$wp_customize->get_section( 'tradiestandard_products_section' )->panel = 'tradiestandard_front_page_sections';

}

add_action( 'customize_register', 'tradiestandard_products_controls_customize_register' );
