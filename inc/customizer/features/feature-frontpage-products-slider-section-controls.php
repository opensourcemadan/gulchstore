<?php
/**
 * Customizer functionality for the Products Slider Section.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

/**
 * Hook controls for Products Slider Section to Customizer.
 */
function tradiestandard_products_slider_controls_customize_register( $wp_customize ) {

	$tradiestandard_require_woo = '';
	if ( ! class_exists( 'WooCommerce' ) ) {
		$tradiestandard_require_woo = '<div class="tradiestandard-require-woo"><p>' . sprintf(
			/* translators: 1: Link to WooCommerce Plugin */
				__( 'To use this section, you are required to first install the  %1$s plugin', 'tradiestandard' ),
			sprintf(
				/* translators: 1: Link to WiooCommerce Plugin. 2: 'WooCommerce' */
				'<a href="' . esc_url( wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=woocommerce' ),
				'install-plugin_woocommerce' ) ) . '">%s</a>',
				esc_html__( 'WooCommerce', 'tradiestandard' )
			)
		) . '</p></div>';
	}

	/*  Products slider section */

	$wp_customize->add_section( 'tradiestandard_products_slider_section', array(
		'title'       => __( 'Products slider section', 'tradiestandard' ),
		'description' => $tradiestandard_require_woo,
		'priority'    => apply_filters( 'tradiestandard_section_priority', 35, 'tradiestandard_products_slider_section' ),
	) );

	/* Hide products slider on frontpage */
	$wp_customize->add_setting( 'tradiestandard_products_slider_hide', array(
		'default'           => false,
		'sanitize_callback' => 'tradiestandard_sanitize_checkbox',
	) );

	$wp_customize->add_control(
		'tradiestandard_products_slider_hide',
		array(
			'type'     => 'checkbox',
			'label'    => __( 'Hide products slider section on frontpage?', 'tradiestandard' ),
			'section'  => 'tradiestandard_products_slider_section',
			'priority' => 1,
		)
	);

	/* Hide products slider on single product page */
	$wp_customize->add_setting( 'tradiestandard_products_slider_single_hide', array(
		'transport'         => 'postMessage',
		'sanitize_callback' => 'tradiestandard_sanitize_text',
	) );

	$wp_customize->add_control(
		'tradiestandard_products_slider_single_hide',
		array(
			'type'     => 'checkbox',
			'label'    => __( 'Hide products slider section on single product page?', 'tradiestandard' ),
			'section'  => 'tradiestandard_products_slider_section',
			'priority' => 2,
		)
	);

	/* Title */
	$wp_customize->add_setting( 'tradiestandard_products_slider_title', array(
		'transport'         => 'postMessage',
		'sanitize_callback' => 'tradiestandard_sanitize_text',
		'default'           => __( 'Exclusive products', 'tradiestandard' ),
	) );

	$wp_customize->add_control( 'tradiestandard_products_slider_title', array(
		'label'    => __( 'Section title', 'tradiestandard' ),
		'section'  => 'tradiestandard_products_slider_section',
		'priority' => 3,
	) );

	/* Subtitle */
	$wp_customize->add_setting( 'tradiestandard_products_slider_subtitle', array(
		'transport'         => 'postMessage',
		'sanitize_callback' => 'tradiestandard_sanitize_text',
		'default'           => __( 'Special category of products', 'tradiestandard' ),
	) );

	$wp_customize->add_control( 'tradiestandard_products_slider_subtitle', array(
		'label'    => __( 'Section subtitle', 'tradiestandard' ),
		'section'  => 'tradiestandard_products_slider_section',
		'priority' => 4,
	) );

	/* Category */
	$tradiestandard_prod_categories_array = array(
		'-' => __( 'Select category', 'tradiestandard' ),
	);

	$tradiestandard_prod_categories = get_categories( array(
		'taxonomy'   => 'product_cat',
		'hide_empty' => 0,
		'title_li'   => '',
	) );

	if ( ! empty( $tradiestandard_prod_categories ) ) :
		foreach ( $tradiestandard_prod_categories as $tradiestandard_prod_cat ) :

			if ( ! empty( $tradiestandard_prod_cat->term_id ) && ! empty( $tradiestandard_prod_cat->name ) ) :
				$tradiestandard_prod_categories_array[ $tradiestandard_prod_cat->term_id ] = $tradiestandard_prod_cat->name;
			endif;

		endforeach;
	endif;

	$wp_customize->add_setting( 'tradiestandard_products_slider_category', array(
		'sanitize_callback' => 'tradiestandard_sanitize_text',
	) );
	$wp_customize->add_control(
		'tradiestandard_products_slider_category',
		array(
			'type'        => 'select',
			'label'       => __( 'Products category', 'tradiestandard' ),
			'section'     => 'tradiestandard_products_slider_section',
			'choices'     => $tradiestandard_prod_categories_array,
			'priority'    => 5,
			'description' => __( 'If no category is selected , WooCommerce products from the first category found are displaying.', 'tradiestandard' ),
		)
	);

	$wp_customize->get_section( 'tradiestandard_products_slider_section' )->panel = 'tradiestandard_front_page_sections';

}

add_action( 'customize_register', 'tradiestandard_products_slider_controls_customize_register' );
