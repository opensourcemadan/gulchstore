<?php
/**
 * Customizer functionality for the slider Section.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

/**
 * Hook controls for slider Section to Customizer.
 */
function tradiestandard_slider_controls_customize_register( $wp_customize ) {

	/* slider section */

	$wp_customize->add_section( 'tradiestandard_slider_section', array(
		'title'    => __( 'slider section', 'tradiestandard' ),
		'priority' => apply_filters( 'tradiestandard_section_priority', 10, 'tradiestandard_slider_section' ),
	) );

	/* Hide Slider */
	$wp_customize->add_setting( 'tradiestandard_slider_hide', array(
		'transport'         => 'postMessage',
		'sanitize_callback' => 'tradiestandard_sanitize_text',
	) );

	$wp_customize->add_control(
		'tradiestandard_slider_hide',
		array(
			'type'        => 'checkbox',
			'label'       => __( 'Hide slider section?', 'tradiestandard' ),
			'section'     => 'tradiestandard_slider_section',
			'priority'    => 1,
		)
	);

	/* Slider */
	$wp_customize->add_setting( 'tradiestandard_slider', array(
		'transport'         => 'postMessage',
		'sanitize_callback' => 'tradiestandard_sanitize_repeater',
		'default'           => json_encode( array(
			array(
				'image_url' => get_template_directory_uri() . '/assets/images/slide1.jpg',
				'link'      => '#',
				'text'      => __( 'Rack', 'tradiestandard' ),
				'subtext'   => __( 'View our latest Product', 'tradiestandard' ),
				'label'     => __( 'Read more', 'tradiestandard' ),
			),
			array(
				'image_url' => get_template_directory_uri() . '/assets/images/slide1.jpg',
				'link'      => '#',
				'text'      => __( 'Rack', 'tradiestandard' ),
				'subtext'   => __( 'Catlog Theme', 'tradiestandard' ),
				'label'     => __( 'Read more', 'tradiestandard' ),
			),
			array(
				'image_url' => get_template_directory_uri() . '/assets/images/slide1.jpg',
				'link'      => '#',
				'text'      => __( 'Rack', 'tradiestandard' ),
				'subtext'   => __( 'New Arriaval', 'tradiestandard' ),
				'label'     => __( 'Read more', 'tradiestandard' ),
			),
		) ),
	) );
	$wp_customize->add_control( new tradiestandard_Repeater_Controler( $wp_customize, 'tradiestandard_slider', array(
		'label'                         => __( 'Add new Slider', 'tradiestandard' ),
		'section'                       => 'tradiestandard_slider_section',
		'active_callback'               => 'is_front_page',
		'priority'                      => 3,
		'tradiestandard_image_control'       => true,
		'tradiestandard_link_control'        => true,
		'tradiestandard_text_control'        => true,
		'tradiestandard_subtext_control'     => true,
		'tradiestandard_label_control'       => true,
		'tradiestandard_icon_control'        => false,
		'tradiestandard_description_control' => false,
		'tradiestandard_box_label'           => __( 'Slider', 'tradiestandard' ),
		'tradiestandard_box_add_label'       => __( 'Add new Slider', 'tradiestandard' ),
	) ) );

	$wp_customize->get_section( 'tradiestandard_slider_section' )->panel = 'tradiestandard_front_page_sections';

}

add_action( 'customize_register', 'tradiestandard_slider_controls_customize_register' );
