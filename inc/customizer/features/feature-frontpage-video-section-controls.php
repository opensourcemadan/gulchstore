<?php
/**
 * Customizer functionality for the Video Section.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

/**
 * Hook controls for Video Section to Customizer.
 */
function tradiestandard_video_controls_customize_register( $wp_customize ) {

	/* Video section */

	$wp_customize->add_section( 'tradiestandard_video_section', array(
		'title'    => __( 'Video section', 'tradiestandard' ),
		'priority' => apply_filters( 'tradiestandard_section_priority', 25, 'tradiestandard_video_section' ),
	) );

	/* Hide video */
	$wp_customize->add_setting( 'tradiestandard_video_hide', array(
		'default'           => false,
		'transport'         => 'postMessage',
		'sanitize_callback' => 'tradiestandard_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'tradiestandard_video_hide', array(
		'type'        => 'checkbox',
		'label'       => __( 'Hide video section?', 'tradiestandard' ),
		'section'     => 'tradiestandard_video_section',
		'priority'    => 1,
	) );

	/* Title */
	$wp_customize->add_setting( 'tradiestandard_video_title', array(
		'sanitize_callback' => 'tradiestandard_sanitize_text',
		'transport'         => 'postMessage',
	) );

	$wp_customize->add_control( 'tradiestandard_video_title', array(
		'label'    => __( 'Title', 'tradiestandard' ),
		'section'  => 'tradiestandard_video_section',
		'priority' => 2,
	) );

	/* Youtube link */
	$wp_customize->add_setting( 'tradiestandard_yt_link', array(
		'sanitize_callback' => 'esc_url',
	) );

	$wp_customize->add_control( 'tradiestandard_yt_link', array(
		'label'    => __( 'Youtube link', 'tradiestandard' ),
		'section'  => 'tradiestandard_video_section',
		'priority' => 3,
	) );

	/* Thumbnail */
	$wp_customize->add_setting( 'tradiestandard_yt_thumbnail', array(
		'sanitize_callback' => 'esc_url',
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'tradiestandard_yt_thumbnail', array(
		'label'       => __( 'Video thumbnail', 'tradiestandard' ),
		'section'     => 'tradiestandard_video_section',
		'priority'    => 4,
	) ) );

	$wp_customize->get_section( 'tradiestandard_video_section' )->panel = 'tradiestandard_front_page_sections';

}

add_action( 'customize_register', 'tradiestandard_video_controls_customize_register' );
