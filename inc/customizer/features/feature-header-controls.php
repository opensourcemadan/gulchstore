<?php
/**
 * Customizer functionality for the Header Section.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

/**
 * Hook controls for Header Section section to Customizer.
 */
function tradiestandard_header_controls_customize_register( $wp_customize ) {

	/*  Header */

	$wp_customize->add_section( 'tradiestandard_header_section', array(
		'title'    => __( 'Header', 'tradiestandard' ),
		'priority' => 40,
	) );

	/* Logo */
	$wp_customize->add_setting( 'tradiestandard_logo', array(
		'transport'         => 'postMessage',
		'sanitize_callback' => 'esc_url',
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'tradiestandard_logo', array(
		'label'    => __( 'Logo', 'tradiestandard' ),
		'section'  => 'title_tagline',
		'priority' => 1,
	) ) );

	$wp_customize->get_control( 'header_image' )->section  = 'tradiestandard_header_section';
	$wp_customize->get_control( 'header_image' )->priority = '2';

}

add_action( 'customize_register', 'tradiestandard_header_controls_customize_register' );
