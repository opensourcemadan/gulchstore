<<<<<<< HEAD
<?php
/**
 * tradiestandard setup functions
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

define( 'tradiestandard_PHP_INCLUDE',  get_template_directory() . '/inc' );

/**
 * Assign the tradiestandard version to a var
 */

if ( ! defined( 'SI_VERSION' ) ) {
	$theme_ver = wp_get_theme()->get( 'Version' );
	define( 'SI_VERSION', $theme_ver );
}

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function tradiestandard_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'tradiestandard_content_width', 980 );
}
add_action( 'after_setup_theme', 'tradiestandard_content_width', 0 );

/**
 * Define Allowed Files to be included.
 */
function tradiestandard_filter_features( $array ) {
	return array_merge( $array, array(
		'/customizer/features/feature-header-controls',
		'/customizer/features/feature-footer-controls',
		'/customizer/features/feature-advanced-controls',

		'/customizer/features/feature-frontpage-big-title-section-controls',
		'/customizer/features/feature-frontpage-slider-section-controls',
		'/customizer/features/feature-frontpage-banners-section-controls',
		'/customizer/features/feature-frontpage-products-section-controls',
		'/customizer/features/feature-frontpage-video-section-controls',
		'/customizer/features/feature-frontpage-products-slider-section-controls',

		'/customizer/features/feature-blog-header-controls',
		'/customizer/features/feature-contact-controls',
		'/customizer/features/feature-404-controls'
	));
}
add_filter( 'tradiestandard_filter_features', 'tradiestandard_filter_features' );

/**
 * Include features files.
 */
function tradiestandard_include_features() {
	$tradiestandard_inc_dir = rtrim( tradiestandard_PHP_INCLUDE, '/' );
	$tradiestandard_allowed_phps = array();
	$tradiestandard_allowed_phps = apply_filters( 'tradiestandard_filter_features',$tradiestandard_allowed_phps );
	foreach ( $tradiestandard_allowed_phps as $file ) {
		$tradiestandard_file_to_include = $tradiestandard_inc_dir . $file . '.php';
		if ( file_exists( $tradiestandard_file_to_include ) ) {
			include_once( $tradiestandard_file_to_include );
		}
	}
}
add_action( 'after_setup_theme','tradiestandard_include_features' );

if ( ! function_exists( 'tradiestandard_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function tradiestandard_setup() {
		/*
		 * Load Localisation files.
		 *
		 * Note: the first-loaded translation file overrides any following ones if the same translation is present.
		 */

		// wp-content/languages/themes/tradiestandard-
		load_theme_textdomain( 'tradiestandard', trailingslashit( WP_LANG_DIR ) . 'themes/' );

		// wp-content/themes/child-theme-name/languages/
		load_theme_textdomain( 'tradiestandard', get_stylesheet_directory() . '/languages' );

		// wp-content/themes/theme-name/languages/
		load_theme_textdomain( 'tradiestandard', get_template_directory() . '/languages' );

		/**
		 * Add default posts and comments RSS feed links to head.
		 */
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'tradiestandard_blog_image_size', 750, 500, true );
		add_image_size( 'tradiestandard_banner_homepage', 360, 235, true );
		add_image_size( 'tradiestandard_category_thumbnail', 500, 500, true );
		add_image_size( 'tradiestandard_cart_item_image_size', 58, 72, true );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus( array(
			'primary'		=> __( 'Primary Menu', 'tradiestandard' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, comments, galleries, captions and widgets
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'widgets',
		) );

		// Setup the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'tradiestandard_custom_background_args', array(
			'default-color' => apply_filters( 'tradiestandard_default_background_color', 'fcfcfc' ),
			'default-image' => '',
		) ) );

		// Declare WooCommerce support
		add_theme_support( 'woocommerce' );

		// Declare support for title theme feature
		add_theme_support( 'title-tag' );

		/* Custom header */
		add_theme_support( 'custom-header', array(
			'default-image' => get_template_directory_uri() . '/assets/images/header.jpg',
			'width'         => 2048,
			'height'        => 280,
			'flex-height'   => true,
			'flex-width'    => true,
		));

		/* tgm-plugin-activation */
		require_once get_template_directory() . '/class-tgm-plugin-activation.php';

		if ( class_exists( 'WooCommerce' ) ) {
			add_theme_support( 'wc-product-gallery-zoom' );
			add_theme_support( 'wc-product-gallery-lightbox' );
			add_theme_support( 'wc-product-gallery-slider' );
		}
	}
endif; // tradiestandard_setup

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function tradiestandard_widgets_init() {

	register_sidebar( array(
		'name'          => __( 'Sidebar', 'tradiestandard' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer area 1', 'tradiestandard' ),
		'id'            => 'sidebar-footer-area-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer area 2', 'tradiestandard' ),
		'id'            => 'sidebar-footer-area-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer area 3', 'tradiestandard' ),
		'id'            => 'sidebar-footer-area-3',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer area 4', 'tradiestandard' ),
		'id'            => 'sidebar-footer-area-4',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Sidebar Shop Page', 'tradiestandard' ),
		'id'            => 'tradiestandard-sidebar-shop-archive',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

}

/**
 * Enqueue scripts and styles.
 *
 * @since  1.0.0
 */
function tradiestandard_scripts() {

	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css', array(), '20120206', 'all' );

	wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/assets/css/vendor/magnific-popup.css', array(), '20120206', 'all' );

	wp_enqueue_style( 'flexslider', get_template_directory_uri() . '/assets/css/vendor/flexslider.css', array( 'magnific-popup' ), '20120206', 'all' );

	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/assets/css/vendor/owl.carousel.min.css', array( 'flexslider' ), '2.1.6', 'all' );

	wp_enqueue_style( 'tradiestandard-animate', get_template_directory_uri() . '/assets/css/vendor/animate.css', array( 'owl-carousel' ), '20120206', 'all' );

	wp_enqueue_style( 'tradiestandard-main-style', get_template_directory_uri() . '/assets/css/style.css', array( 'bootstrap' ), SI_VERSION, 'all' );

	wp_enqueue_style( 'tradiestandard-style', get_stylesheet_uri(), '', SI_VERSION );

	wp_enqueue_script( 'jquery' );

	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'jquery-mb-YTPlayer', get_template_directory_uri() . '/assets/js/vendor/jquery.mb.YTPlayer.min.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'jqBootstrapValidation', get_template_directory_uri() . '/assets/js/vendor/jqBootstrapValidation.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'flexslider', get_template_directory_uri() . '/assets/js/vendor/jquery.flexslider-min.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/assets/js/vendor/jquery.magnific-popup.min.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'fitvids', get_template_directory_uri() . '/assets/js/vendor/jquery.fitvids.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'smoothscroll', get_template_directory_uri() . '/assets/js/vendor/smoothscroll.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri() . '/assets/js/vendor/owl.carousel.min.js', array( 'jquery' ), '2.1.6', true );

	wp_enqueue_script( 'tradiestandard-custom', get_template_directory_uri() . '/assets/js/custom.js', array( 'jquery', 'flexslider', 'jquery-mb-YTPlayer' ), '20120206', true );

	wp_enqueue_script( 'tradiestandard-navigation', get_template_directory_uri() . '/js/navigation.min.js', array(), '20120207', true );

	wp_enqueue_script( 'tradiestandard-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.min.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

/**
 * Enqueue Admin Styles
 */
function tradiestandard_admin_styles() {
	wp_enqueue_media();
	wp_enqueue_style( 'tradiestandard_admin_stylesheet', get_template_directory_uri() . '/assets/css/admin-style.css' );
}

add_action( 'tgmpa_register', 'tradiestandard_register_required_plugins' );

/**
 * Register TGMP Required Plugins
 */
function tradiestandard_register_required_plugins() {

	$plugins = array(
				array(
					'name'      => 'WooCommerce',
					'slug'      => 'woocommerce',
					'required'  => false,
				),
			);

	$config = array(
		'default_path' => '',
		'menu' => 'tgmpa-install-plugins',
		'has_notices' => true,
		'dismissable' => true,
		'dismiss_msg' => '',
		'is_automatic' => false,
		'message' => '',
		);
	tgmpa( $plugins, $config );
}


/**
 * Add ID-s for migration
 */
function tradiestandard_add_id() {

	$migrate = get_option( 'tradiestandard_migrate_translation' );

	if ( isset( $migrate ) && $migrate == false ) {

		/* Slider section */
		$tradiestandard_slider = get_theme_mod('tradiestandard_slider', json_encode(
			array(
				array(
					'image_url' => get_template_directory_uri() . '/assets/images/slide1.jpg',
					'link' => '#',
					'text' => __( 'Rask','tradiestandard' ),
					'subtext' => __( 'Tradie Theme','tradiestandard' ),
					'label' => __( 'Read more','tradiestandard' ),
				),
				array(
					'image_url' => get_template_directory_uri() . '/assets/images/slide2.jpg',
					'link' => '#',
					'text' => __( 'Rask','tradiestandard' ),
					'subtext' => __( 'Tradie Theme','tradiestandard' ),
					'label' => __( 'Read more','tradiestandard' ),
				),
				array(
					'image_url' => get_template_directory_uri() . '/assets/images/slide3.jpg',
					'link' => '#',
					'text' => __( 'Rask','tradiestandard' ),
					'subtext' => __( 'Tradie Theme','tradiestandard' ),
					'label' => __( 'Read more','tradiestandard' ),
				),
			)
		));

		if ( ! empty( $tradiestandard_slider ) ) {

			$tradiestandard_slider_decoded = json_decode( $tradiestandard_slider );
			foreach ( $tradiestandard_slider_decoded as &$it ) {
				if ( ! array_key_exists( 'id' , $it ) || ! ($it->id) ) {
					$it = (object) array_merge( (array) $it, array(
						'id' => 'tradiestandard_' . uniqid(),
					) );
				}
			}

			$tradiestandard_slider = json_encode( $tradiestandard_slider_decoded );
			set_theme_mod( 'tradiestandard_slider', $tradiestandard_slider );
		}

		/* Banners section */
		$tradiestandard_banners = get_theme_mod('tradiestandard_banners', json_encode(
			array(
				array(
					'image_url' => get_template_directory_uri() . '/assets/images/banner1.jpg',
					'link' => '#',
				),
				array(
					'image_url' => get_template_directory_uri() . '/assets/images/banner2.jpg',
					'link' => '#',
				),
				array(
					'image_url' => get_template_directory_uri() . '/assets/images/banner3.jpg',
					'link' => '#',
				),
			)
		));

		if ( ! empty( $tradiestandard_banners ) ) {

			$tradiestandard_banners_decoded = json_decode( $tradiestandard_banners );
			foreach ( $tradiestandard_banners_decoded as &$it ) {
				if ( ! array_key_exists( 'id' , $it ) || ! ($it->id) ) {
					$it = (object) array_merge( (array) $it, array(
						'id' => 'tradiestandard_' . uniqid(),
					) );
				}
			}

			$tradiestandard_banners = json_encode( $tradiestandard_banners_decoded );
			set_theme_mod( 'tradiestandard_banners', $tradiestandard_banners );
		}

		/* Footer socials */
		$tradiestandard_socials = get_theme_mod('tradiestandard_socials', json_encode(
			array(
				array(
					'icon_value' => 'social_facebook',
					'link' => '#',
				),
				array(
					'icon_value' => 'social_twitter',
					'link' => '#',
				),
				array(
					'icon_value' => 'social_dribbble',
					'link' => '#',
				),
				array(
					'icon_value' => 'social_skype',
					'link' => '#',
				),
			)
		));

		if ( ! empty( $tradiestandard_socials ) ) {

			$tradiestandard_socials_decoded = json_decode( $tradiestandard_socials );
			foreach ( $tradiestandard_socials_decoded as &$it ) {
				if ( ! array_key_exists( 'id' , $it ) || ! ($it->id) ) {
					$it = (object) array_merge( (array) $it, array(
						'id' => 'tradiestandard_' . uniqid(),
					) );
				}
			}

			$tradiestandard_socials = json_encode( $tradiestandard_socials_decoded );
			set_theme_mod( 'tradiestandard_socials', $tradiestandard_socials );
		}

		update_option( 'tradiestandard_migrate_translation', true );
	}// End if().
}
add_action( 'shutdown', 'tradiestandard_add_id' );

/* Polylang repeater translate */

if ( function_exists( 'icl_unregister_string' ) && function_exists( 'icl_register_string' ) ) {

	/* Slider section */

	$tradiestandard_slider_pl = get_theme_mod( 'tradiestandard_slider' );

	if ( ! empty( $tradiestandard_slider_pl ) ) {

		$tradiestandard_slider_pl_decoded = json_decode( $tradiestandard_slider_pl );

		if ( ! empty( $tradiestandard_slider_pl_decoded ) ) {

			foreach ( $tradiestandard_slider_pl_decoded as $tradiestandard_slider ) {

				if ( ! empty( $tradiestandard_slider->id ) ) {
					$id = $tradiestandard_slider->id;
				}
				$text = $tradiestandard_slider->text;
				$subtext = $tradiestandard_slider->subtext;
				$image_url = $tradiestandard_slider->image_url;
				$link = $tradiestandard_slider->link;
				$label = $tradiestandard_slider->label;

				if ( ! empty( $id ) ) {

					if ( ! empty( $image_url ) ) {
						icl_unregister_string( 'Slide ' . $id, 'Slide image' );
						icl_register_string( 'Slide ' . $id, 'Slide image', $image_url );
					} else {
						icl_unregister_string( 'Slide ' . $id, 'Slide image' );
					}

					if ( ! empty( $text ) ) {
						icl_unregister_string( 'Slide ' . $id, 'Slide text' );
						icl_register_string( 'Slide ' . $id, 'Slide text', $text );
					} else {
						icl_unregister_string( 'Slide ' . $id, 'Slide text' );
					}

					if ( ! empty( $subtext ) ) {
						icl_unregister_string( 'Slide ' . $id, 'Slide subtext' );
						icl_register_string( 'Slide ' . $id, 'Slide subtext',$subtext );
					} else {
						icl_unregister_string( 'Slide ' . $id, 'Slide subtext' );
					}

					if ( ! empty( $link ) ) {
						icl_unregister_string( 'Slide ' . $id, 'Slide button link' );
						icl_register_string( 'Slide ' . $id, 'Slide button link', $link );
					} else {
						icl_unregister_string( 'Slide ' . $id, 'Slide button link' );
					}

					if ( ! empty( $label ) ) {
						icl_unregister_string( 'Slide ' . $id, 'Slide button label' );
						icl_register_string( 'Slide ' . $id, 'Slide button label', $label );
					} else {
						icl_unregister_string( 'Slide ' . $id, 'Slide button label' );
					}
				}// End if().
			}// End foreach().
		}// End if().
	}// End if().

	/* Banners section */

	$tradiestandard_banners_pl = get_theme_mod( 'tradiestandard_banners' );

	if ( ! empty( $tradiestandard_banners_pl ) ) {

		$tradiestandard_banners_pl_decoded = json_decode( $tradiestandard_banners_pl );

		if ( ! empty( $tradiestandard_banners_pl_decoded ) ) {

			foreach ( $tradiestandard_banners_pl_decoded as $tradiestandard_banners ) {

				if ( ! empty( $tradiestandard_banners->id ) ) {
					$id = $tradiestandard_banners->id;
				}

				$image_url = $tradiestandard_banners->image_url;
				$link = $tradiestandard_banners->link;

				if ( ! empty( $id ) ) {

					if ( ! empty( $link ) ) {
						icl_unregister_string( 'Banner ' . $id, 'Banner link' );
						icl_register_string( 'Banner ' . $id, 'Banner link', $link );
					} else {
						icl_unregister_string( 'Banner ' . $id, 'Banner link' );
					}

					if ( ! empty( $image_url ) ) {
						icl_unregister_string( 'Banner ' . $id, 'Banner image' );
						icl_register_string( 'Banner ' . $id, 'Banner image', $image_url );
					} else {
						icl_unregister_string( 'Banner ' . $id, 'Banner image' );
					}
				}
			}
		}
	}

	/*Footer socials */

	$tradiestandard_socials_pl = get_theme_mod( 'tradiestandard_socials' );

	if ( ! empty( $tradiestandard_socials_pl ) ) {

		$tradiestandard_socials_pl_decoded = json_decode( $tradiestandard_socials_pl );

		if ( ! empty( $tradiestandard_socials_pl_decoded ) ) {

			foreach ( $tradiestandard_socials_pl_decoded as $tradiestandard_socials ) {

				if ( ! empty( $tradiestandard_socials->id ) ) {
					$id = $tradiestandard_socials->id;
				}
				$icon_value = $tradiestandard_socials->icon_value;
				$link = $tradiestandard_socials->link;

				if ( ! empty( $id ) ) {
					if ( ! empty( $icon_value ) ) {
						icl_unregister_string( 'Social ' . $id, 'Social icon' );
						icl_register_string( 'Social ' . $id, 'Social icon', $icon_value );
					} else {
						icl_unregister_string( 'Social ' . $id, 'Social icon' );
					}
					if ( ! empty( $link ) ) {
						icl_unregister_string( 'Social ' . $id, 'Social link' );
						icl_register_string( 'Social ' . $id, 'Social link', $link );
					} else {
						icl_unregister_string( 'Social ' . $id, 'Social link' );
					}
				}
			}
		}
	}

=======
<?php
/**
 * tradiestandard setup functions
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

define( 'tradiestandard_PHP_INCLUDE',  get_template_directory() . '/inc' );

/**
 * Assign the tradiestandard version to a var
 */

if ( ! defined( 'SI_VERSION' ) ) {
	$theme_ver = wp_get_theme()->get( 'Version' );
	define( 'SI_VERSION', $theme_ver );
}

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function tradiestandard_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'tradiestandard_content_width', 980 );
}
add_action( 'after_setup_theme', 'tradiestandard_content_width', 0 );

/**
 * Define Allowed Files to be included.
 */
function tradiestandard_filter_features( $array ) {
	return array_merge( $array, array(
		'/customizer/features/feature-header-controls',
		'/customizer/features/feature-footer-controls',
		'/customizer/features/feature-advanced-controls',

		'/customizer/features/feature-frontpage-big-title-section-controls',
		'/customizer/features/feature-frontpage-slider-section-controls',
		'/customizer/features/feature-frontpage-banners-section-controls',
		'/customizer/features/feature-frontpage-products-section-controls',
		'/customizer/features/feature-frontpage-video-section-controls',
		'/customizer/features/feature-frontpage-products-slider-section-controls',

		'/customizer/features/feature-blog-header-controls',
		'/customizer/features/feature-contact-controls',
		'/customizer/features/feature-404-controls'
	));
}
add_filter( 'tradiestandard_filter_features', 'tradiestandard_filter_features' );

/**
 * Include features files.
 */
function tradiestandard_include_features() {
	$tradiestandard_inc_dir = rtrim( tradiestandard_PHP_INCLUDE, '/' );
	$tradiestandard_allowed_phps = array();
	$tradiestandard_allowed_phps = apply_filters( 'tradiestandard_filter_features',$tradiestandard_allowed_phps );
	foreach ( $tradiestandard_allowed_phps as $file ) {
		$tradiestandard_file_to_include = $tradiestandard_inc_dir . $file . '.php';
		if ( file_exists( $tradiestandard_file_to_include ) ) {
			include_once( $tradiestandard_file_to_include );
		}
	}
}
add_action( 'after_setup_theme','tradiestandard_include_features' );

if ( ! function_exists( 'tradiestandard_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function tradiestandard_setup() {
		/*
		 * Load Localisation files.
		 *
		 * Note: the first-loaded translation file overrides any following ones if the same translation is present.
		 */

		// wp-content/languages/themes/tradiestandard-
		load_theme_textdomain( 'tradiestandard', trailingslashit( WP_LANG_DIR ) . 'themes/' );

		// wp-content/themes/child-theme-name/languages/
		load_theme_textdomain( 'tradiestandard', get_stylesheet_directory() . '/languages' );

		// wp-content/themes/theme-name/languages/
		load_theme_textdomain( 'tradiestandard', get_template_directory() . '/languages' );

		/**
		 * Add default posts and comments RSS feed links to head.
		 */
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'tradiestandard_blog_image_size', 750, 500, true );
		add_image_size( 'tradiestandard_banner_homepage', 360, 235, true );
		add_image_size( 'tradiestandard_category_thumbnail', 500, 500, true );
		add_image_size( 'tradiestandard_cart_item_image_size', 58, 72, true );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus( array(
			'primary'		=> __( 'Primary Menu', 'tradiestandard' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, comments, galleries, captions and widgets
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'widgets',
		) );

		// Setup the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'tradiestandard_custom_background_args', array(
			'default-color' => apply_filters( 'tradiestandard_default_background_color', 'fcfcfc' ),
			'default-image' => '',
		) ) );

		// Declare WooCommerce support
		add_theme_support( 'woocommerce' );

		// Declare support for title theme feature
		add_theme_support( 'title-tag' );

		/* Custom header */
		add_theme_support( 'custom-header', array(
			'default-image' => get_template_directory_uri() . '/assets/images/header.jpg',
			'width'         => 2048,
			'height'        => 280,
			'flex-height'   => true,
			'flex-width'    => true,
		));

		/* tgm-plugin-activation */
		require_once get_template_directory() . '/class-tgm-plugin-activation.php';

		if ( class_exists( 'WooCommerce' ) ) {
			add_theme_support( 'wc-product-gallery-zoom' );
			add_theme_support( 'wc-product-gallery-lightbox' );
			add_theme_support( 'wc-product-gallery-slider' );
		}
	}
endif; // tradiestandard_setup

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function tradiestandard_widgets_init() {

	register_sidebar( array(
		'name'          => __( 'Sidebar', 'tradiestandard' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer area 1', 'tradiestandard' ),
		'id'            => 'sidebar-footer-area-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer area 2', 'tradiestandard' ),
		'id'            => 'sidebar-footer-area-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer area 3', 'tradiestandard' ),
		'id'            => 'sidebar-footer-area-3',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer area 4', 'tradiestandard' ),
		'id'            => 'sidebar-footer-area-4',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Sidebar Shop Page', 'tradiestandard' ),
		'id'            => 'tradiestandard-sidebar-shop-archive',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

}

/**
 * Enqueue scripts and styles.
 *
 * @since  1.0.0
 */
function tradiestandard_scripts() {

	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css', array(), '20120206', 'all' );

	wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/assets/css/vendor/magnific-popup.css', array(), '20120206', 'all' );

	wp_enqueue_style( 'flexslider', get_template_directory_uri() . '/assets/css/vendor/flexslider.css', array( 'magnific-popup' ), '20120206', 'all' );

	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/assets/css/vendor/owl.carousel.min.css', array( 'flexslider' ), '2.1.6', 'all' );

	wp_enqueue_style( 'tradiestandard-animate', get_template_directory_uri() . '/assets/css/vendor/animate.css', array( 'owl-carousel' ), '20120206', 'all' );

	wp_enqueue_style( 'tradiestandard-main-style', get_template_directory_uri() . '/assets/css/style.css', array( 'bootstrap' ), SI_VERSION, 'all' );

	wp_enqueue_style( 'tradiestandard-style', get_stylesheet_uri(), '', SI_VERSION );

	wp_enqueue_script( 'jquery' );

	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'jquery-mb-YTPlayer', get_template_directory_uri() . '/assets/js/vendor/jquery.mb.YTPlayer.min.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'jqBootstrapValidation', get_template_directory_uri() . '/assets/js/vendor/jqBootstrapValidation.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'flexslider', get_template_directory_uri() . '/assets/js/vendor/jquery.flexslider-min.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/assets/js/vendor/jquery.magnific-popup.min.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'fitvids', get_template_directory_uri() . '/assets/js/vendor/jquery.fitvids.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'smoothscroll', get_template_directory_uri() . '/assets/js/vendor/smoothscroll.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri() . '/assets/js/vendor/owl.carousel.min.js', array( 'jquery' ), '2.1.6', true );

	wp_enqueue_script( 'tradiestandard-custom', get_template_directory_uri() . '/assets/js/custom.js', array( 'jquery', 'flexslider', 'jquery-mb-YTPlayer' ), '20120206', true );

	wp_enqueue_script( 'tradiestandard-navigation', get_template_directory_uri() . '/js/navigation.min.js', array(), '20120207', true );

	wp_enqueue_script( 'tradiestandard-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.min.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

/**
 * Enqueue Admin Styles
 */
function tradiestandard_admin_styles() {
	wp_enqueue_media();
	wp_enqueue_style( 'tradiestandard_admin_stylesheet', get_template_directory_uri() . '/assets/css/admin-style.css' );
}

add_action( 'tgmpa_register', 'tradiestandard_register_required_plugins' );

/**
 * Register TGMP Required Plugins
 */
function tradiestandard_register_required_plugins() {

	$plugins = array(
				array(
					'name'      => 'WooCommerce',
					'slug'      => 'woocommerce',
					'required'  => false,
				),
			);

	$config = array(
		'default_path' => '',
		'menu' => 'tgmpa-install-plugins',
		'has_notices' => true,
		'dismissable' => true,
		'dismiss_msg' => '',
		'is_automatic' => false,
		'message' => '',
		);
	tgmpa( $plugins, $config );
}


/**
 * Add ID-s for migration
 */
function tradiestandard_add_id() {

	$migrate = get_option( 'tradiestandard_migrate_translation' );

	if ( isset( $migrate ) && $migrate == false ) {

		/* Slider section */
		$tradiestandard_slider = get_theme_mod('tradiestandard_slider', json_encode(
			array(
				array(
					'image_url' => get_template_directory_uri() . '/assets/images/slide1.jpg',
					'link' => '#',
					'text' => __( 'Rask','tradiestandard' ),
					'subtext' => __( 'Tradie Theme','tradiestandard' ),
					'label' => __( 'Read more','tradiestandard' ),
				),
				array(
					'image_url' => get_template_directory_uri() . '/assets/images/slide2.jpg',
					'link' => '#',
					'text' => __( 'Rask','tradiestandard' ),
					'subtext' => __( 'Tradie Theme','tradiestandard' ),
					'label' => __( 'Read more','tradiestandard' ),
				),
				array(
					'image_url' => get_template_directory_uri() . '/assets/images/slide3.jpg',
					'link' => '#',
					'text' => __( 'Rask','tradiestandard' ),
					'subtext' => __( 'Tradie Theme','tradiestandard' ),
					'label' => __( 'Read more','tradiestandard' ),
				),
			)
		));

		if ( ! empty( $tradiestandard_slider ) ) {

			$tradiestandard_slider_decoded = json_decode( $tradiestandard_slider );
			foreach ( $tradiestandard_slider_decoded as &$it ) {
				if ( ! array_key_exists( 'id' , $it ) || ! ($it->id) ) {
					$it = (object) array_merge( (array) $it, array(
						'id' => 'tradiestandard_' . uniqid(),
					) );
				}
			}

			$tradiestandard_slider = json_encode( $tradiestandard_slider_decoded );
			set_theme_mod( 'tradiestandard_slider', $tradiestandard_slider );
		}

		/* Banners section */
		$tradiestandard_banners = get_theme_mod('tradiestandard_banners', json_encode(
			array(
				array(
					'image_url' => get_template_directory_uri() . '/assets/images/banner1.jpg',
					'link' => '#',
				),
				array(
					'image_url' => get_template_directory_uri() . '/assets/images/banner2.jpg',
					'link' => '#',
				),
				array(
					'image_url' => get_template_directory_uri() . '/assets/images/banner3.jpg',
					'link' => '#',
				),
			)
		));

		if ( ! empty( $tradiestandard_banners ) ) {

			$tradiestandard_banners_decoded = json_decode( $tradiestandard_banners );
			foreach ( $tradiestandard_banners_decoded as &$it ) {
				if ( ! array_key_exists( 'id' , $it ) || ! ($it->id) ) {
					$it = (object) array_merge( (array) $it, array(
						'id' => 'tradiestandard_' . uniqid(),
					) );
				}
			}

			$tradiestandard_banners = json_encode( $tradiestandard_banners_decoded );
			set_theme_mod( 'tradiestandard_banners', $tradiestandard_banners );
		}

		/* Footer socials */
		$tradiestandard_socials = get_theme_mod('tradiestandard_socials', json_encode(
			array(
				array(
					'icon_value' => 'social_facebook',
					'link' => '#',
				),
				array(
					'icon_value' => 'social_twitter',
					'link' => '#',
				),
				array(
					'icon_value' => 'social_dribbble',
					'link' => '#',
				),
				array(
					'icon_value' => 'social_skype',
					'link' => '#',
				),
			)
		));

		if ( ! empty( $tradiestandard_socials ) ) {

			$tradiestandard_socials_decoded = json_decode( $tradiestandard_socials );
			foreach ( $tradiestandard_socials_decoded as &$it ) {
				if ( ! array_key_exists( 'id' , $it ) || ! ($it->id) ) {
					$it = (object) array_merge( (array) $it, array(
						'id' => 'tradiestandard_' . uniqid(),
					) );
				}
			}

			$tradiestandard_socials = json_encode( $tradiestandard_socials_decoded );
			set_theme_mod( 'tradiestandard_socials', $tradiestandard_socials );
		}

		update_option( 'tradiestandard_migrate_translation', true );
	}// End if().
}
add_action( 'shutdown', 'tradiestandard_add_id' );

/* Polylang repeater translate */

if ( function_exists( 'icl_unregister_string' ) && function_exists( 'icl_register_string' ) ) {

	/* Slider section */

	$tradiestandard_slider_pl = get_theme_mod( 'tradiestandard_slider' );

	if ( ! empty( $tradiestandard_slider_pl ) ) {

		$tradiestandard_slider_pl_decoded = json_decode( $tradiestandard_slider_pl );

		if ( ! empty( $tradiestandard_slider_pl_decoded ) ) {

			foreach ( $tradiestandard_slider_pl_decoded as $tradiestandard_slider ) {

				if ( ! empty( $tradiestandard_slider->id ) ) {
					$id = $tradiestandard_slider->id;
				}
				$text = $tradiestandard_slider->text;
				$subtext = $tradiestandard_slider->subtext;
				$image_url = $tradiestandard_slider->image_url;
				$link = $tradiestandard_slider->link;
				$label = $tradiestandard_slider->label;

				if ( ! empty( $id ) ) {

					if ( ! empty( $image_url ) ) {
						icl_unregister_string( 'Slide ' . $id, 'Slide image' );
						icl_register_string( 'Slide ' . $id, 'Slide image', $image_url );
					} else {
						icl_unregister_string( 'Slide ' . $id, 'Slide image' );
					}

					if ( ! empty( $text ) ) {
						icl_unregister_string( 'Slide ' . $id, 'Slide text' );
						icl_register_string( 'Slide ' . $id, 'Slide text', $text );
					} else {
						icl_unregister_string( 'Slide ' . $id, 'Slide text' );
					}

					if ( ! empty( $subtext ) ) {
						icl_unregister_string( 'Slide ' . $id, 'Slide subtext' );
						icl_register_string( 'Slide ' . $id, 'Slide subtext',$subtext );
					} else {
						icl_unregister_string( 'Slide ' . $id, 'Slide subtext' );
					}

					if ( ! empty( $link ) ) {
						icl_unregister_string( 'Slide ' . $id, 'Slide button link' );
						icl_register_string( 'Slide ' . $id, 'Slide button link', $link );
					} else {
						icl_unregister_string( 'Slide ' . $id, 'Slide button link' );
					}

					if ( ! empty( $label ) ) {
						icl_unregister_string( 'Slide ' . $id, 'Slide button label' );
						icl_register_string( 'Slide ' . $id, 'Slide button label', $label );
					} else {
						icl_unregister_string( 'Slide ' . $id, 'Slide button label' );
					}
				}// End if().
			}// End foreach().
		}// End if().
	}// End if().

	/* Banners section */

	$tradiestandard_banners_pl = get_theme_mod( 'tradiestandard_banners' );

	if ( ! empty( $tradiestandard_banners_pl ) ) {

		$tradiestandard_banners_pl_decoded = json_decode( $tradiestandard_banners_pl );

		if ( ! empty( $tradiestandard_banners_pl_decoded ) ) {

			foreach ( $tradiestandard_banners_pl_decoded as $tradiestandard_banners ) {

				if ( ! empty( $tradiestandard_banners->id ) ) {
					$id = $tradiestandard_banners->id;
				}

				$image_url = $tradiestandard_banners->image_url;
				$link = $tradiestandard_banners->link;

				if ( ! empty( $id ) ) {

					if ( ! empty( $link ) ) {
						icl_unregister_string( 'Banner ' . $id, 'Banner link' );
						icl_register_string( 'Banner ' . $id, 'Banner link', $link );
					} else {
						icl_unregister_string( 'Banner ' . $id, 'Banner link' );
					}

					if ( ! empty( $image_url ) ) {
						icl_unregister_string( 'Banner ' . $id, 'Banner image' );
						icl_register_string( 'Banner ' . $id, 'Banner image', $image_url );
					} else {
						icl_unregister_string( 'Banner ' . $id, 'Banner image' );
					}
				}
			}
		}
	}

	/*Footer socials */

	$tradiestandard_socials_pl = get_theme_mod( 'tradiestandard_socials' );

	if ( ! empty( $tradiestandard_socials_pl ) ) {

		$tradiestandard_socials_pl_decoded = json_decode( $tradiestandard_socials_pl );

		if ( ! empty( $tradiestandard_socials_pl_decoded ) ) {

			foreach ( $tradiestandard_socials_pl_decoded as $tradiestandard_socials ) {

				if ( ! empty( $tradiestandard_socials->id ) ) {
					$id = $tradiestandard_socials->id;
				}
				$icon_value = $tradiestandard_socials->icon_value;
				$link = $tradiestandard_socials->link;

				if ( ! empty( $id ) ) {
					if ( ! empty( $icon_value ) ) {
						icl_unregister_string( 'Social ' . $id, 'Social icon' );
						icl_register_string( 'Social ' . $id, 'Social icon', $icon_value );
					} else {
						icl_unregister_string( 'Social ' . $id, 'Social icon' );
					}
					if ( ! empty( $link ) ) {
						icl_unregister_string( 'Social ' . $id, 'Social link' );
						icl_register_string( 'Social ' . $id, 'Social link', $link );
					} else {
						icl_unregister_string( 'Social ' . $id, 'Social link' );
					}
				}
			}
		}
	}

>>>>>>> 2f60927d06dd186fefafb15442a32abd421ec7aa
}// End if().