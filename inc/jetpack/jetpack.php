<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

/**
 * Add theme support for Infinite Scroll.
 * See: http://jetpack.me/support/infinite-scroll/
 */
function tradiestandard_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'tradiestandard-blog-container',
		'footer'    => 'page',
	) );
}
add_action( 'after_setup_theme', 'tradiestandard_jetpack_setup' );
