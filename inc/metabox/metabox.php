<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'yourprefix_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Demo_CMB2
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/CMB2/CMB2
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */

if ( file_exists( dirname( __FILE__ ) . '/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/init.php';
}

add_action( 'cmb2_admin_init', 'rask_register_taxonomy_metabox' );
/**
 * Hook in and add a metabox to add fields to taxonomy terms
 */
function rask_register_taxonomy_metabox() {
	$prefix = 'rask_term_';

	/**
	 * Metabox to add fields to categories and tags
	 */
	$cmb_term = new_cmb2_box( array(
		'id'               => $prefix . 'edit',
		'title'            => esc_html__( 'Category Metabox', 'cmb2' ), // Doesn't output for term boxes
		'object_types'     => array( 'term' ), // Tells CMB2 to use term_meta vs post_meta
		'taxonomies'       => array( 'product_cat' ), // Tells CMB2 which taxonomies should have these fields
		// 'new_term_section' => true, // Will display in the "Add New Category" section

		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'cmb_styles' => false, // false to disable the CMB stylesheet
	) );

	$cmb_term->add_field( array(
		'name'     => esc_html__( 'Category Banner', 'cmb2' ),
		'desc'     => esc_html__( 'Display in Product Page if there is no banner added', 'cmb2' ),
		'id'       => $prefix . 'extra_info',
		'type'     => 'title',
		'on_front' => false,
	) );

	$cmb_term->add_field( array(
		'name' => esc_html__( 'Category Banner', 'cmb2' ),
		'desc' => esc_html__( 'Upload Banner Image', 'cmb2' ),
		'id'   => $prefix . 'banner',
		'type' => 'file',
	) );

}

// register metabox to product page
if ( is_woocommerce_activated() ) {
	add_action('cmb2_admin_init', 'rask_product_single_banner');
	/**
	 * Hook in and add a metabox to add fields to taxonomy terms
	 */
	function rask_product_single_banner() {
		$prefix = 'rask_ps_';
		/**
		 * Metabox to add fields to categories and tags
		 */
		$cmb_term = new_cmb2_box( array(
			'id'               => $prefix . 'edit',
			'title'            => esc_html__( 'Banner Image', 'cmb2' ), // Doesn't output for term boxes
			'object_types'     => array( 'product' ), // Post type
			'context'    => 'normal',
			'priority'   => 'high',
			'show_names' => true, // Show field names on the left
			'cmb_styles' => false, // false to disable the CMB stylesheet
		) );

		$cmb_term->add_field( array(
			'name'     => esc_html__( 'Product Banner', 'cmb2' ),
			'id'       => $prefix . 'extra_info',
			'type'     => 'title',
			'on_front' => false,
		) );

		$cmb_term->add_field( array(
			'name' => esc_html__( 'Product Banner', 'cmb2' ),
			'desc' => esc_html__( 'Upload Banner Image', 'cmb2' ),
			'id'   => $prefix . 'top_banner',
			'type' => 'file',
		) );
	}
}

// repeatable field for product tab
function register_metabox() {
    $prefix = 'repeatable_tab_';

    $cmb_tab = new_cmb2_box( array(
        'id'            => $prefix . 'metabox',
        'title'         => __( 'Product Tab', 'cmb2' ),
        'object_types'  => array( 'product' ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );

    // Repeatable group
    $group_field = $cmb_tab->add_field( array(
        'id'          => $prefix . 'sections',
        'type'        => 'group',
        'options'     => array(
            'group_title'   => __( 'Tab', 'cmb2' ) . ' {#}', // {#} gets replaced by row number
            'add_button'    => __( 'Add another tab', 'cmb2' ),
            'remove_button' => __( 'Remove tab', 'cmb2' ),
            'sortable'      => true, // beta
        ),
    ) );

    // Tab Title
    $cmb_tab->add_group_field( $group_field, array(
		'name' => 'Tab Title',
		'id'   => 'tab_title',
		'type' => 'text',
	) );

    // Tab editor
    $cmb_tab->add_group_field( $group_field, array(
        'name'    => __( 'Content', 'cmb2' ),
        'id'      => 'tab_content',
        'type'    => 'wysiwyg',
        'options' => array( 'textarea_rows' => 8, ),
    ) );
}

add_action( 'cmb2_init', 'register_metabox' );