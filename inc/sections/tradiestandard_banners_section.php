<?php
/**
 * The Banners Section
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

		/* BANNERS */


		$tradiestandard_banners_hide = get_theme_mod( 'tradiestandard_banners_hide' );
		$tradiestandard_banners_title = get_theme_mod( 'tradiestandard_banners_title' );

if ( isset( $tradiestandard_banners_hide ) && $tradiestandard_banners_hide != 1 ) :
	echo '<section class="module-small home-banners">';
		elseif ( is_customize_preview() ) :
			echo '<section class="module-small home-banners tradiestandard_hidden_if_not_customizer">';
		endif;

		if ( ( isset( $tradiestandard_banners_hide ) && $tradiestandard_banners_hide != 1) || is_customize_preview() ) :

			$tradiestandard_banners = get_theme_mod( 'tradiestandard_banners', json_encode( array(
				array(
					'image_url' => get_template_directory_uri() . '/assets/images/banner1.jpg',
					'link' => '#',
				),
				array(
					'image_url' => get_template_directory_uri() . '/assets/images/banner2.jpg',
					'link' => '#',
				),
				array(
					'image_url' => get_template_directory_uri() . '/assets/images/banner3.jpg',
					'link' => '#',
				),
			) ) );

			if ( ! empty( $tradiestandard_banners ) ) :

				$tradiestandard_banners_decoded = json_decode( $tradiestandard_banners );

				if ( ! empty( $tradiestandard_banners_decoded ) ) :

						echo '<div class="container">';

					if ( ! empty( $tradiestandard_banners_title ) ) {
						echo '<div class="row">';
						echo '<div class="col-sm-6 col-sm-offset-3">';
						echo '<h2 class="module-title font-alt product-banners-title">' . $tradiestandard_banners_title . '</h2>';
						echo '</div>';
						echo '</div>';

					} elseif ( is_customize_preview() ) {
						echo '<div class="row">';
						echo '<div class="col-sm-6 col-sm-offset-3">';
						echo '<h2 class="module-title font-alt product-banners-title tradiestandard_hidden_if_not_customizer"></h2>';
						echo '</div>';
						echo '</div>';
					}

							echo '<div id="brand_slider" class="row owl-carousel tradiestandard_bannerss_section">';

					foreach ( $tradiestandard_banners_decoded as $tradiestandard_banner ) :

						if ( ! empty( $tradiestandard_banner->image_url ) ) {

							echo '<div><div class="content-box mt-0 mb-0"><div class="content-box-image">';

							if ( ! empty( $tradiestandard_banner->link ) ) {

								echo '<a href="' . esc_url( $tradiestandard_banner->link ) . '"><img src="' . esc_url( $tradiestandard_banner->image_url ) . '"></a>';
							} else {
								echo '<a><img src="' . esc_url( $tradiestandard_banner->image_url ) . '"></a>';
							}
							echo '</div></div></div>';

						}

								endforeach;

							echo '</div>';

						echo '</div>';

				endif;

			endif;

			echo '</section>';

			echo '<hr class="divider-w">';

		endif;	/* END BANNERS */


