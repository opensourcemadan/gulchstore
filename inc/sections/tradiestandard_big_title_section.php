<?php
/**
 * Big title section
 *
 * @package tradiestandard
 * @since 1.0.0
 */

$tradiestandard_big_title_hide = get_theme_mod( 'tradiestandard_big_title_hide' );

if ( isset( $tradiestandard_big_title_hide ) && $tradiestandard_big_title_hide != 1 ) {
	echo '<section id="home" class="home-section home-parallax home-fade home-full-height">';
} elseif ( is_customize_preview() ) {
	echo '<section id="home" class="home-section home-parallax home-fade home-full-height tradiestandard_hidden_if_not_customizer">';
}

if ( ( isset( $tradiestandard_big_title_hide ) && $tradiestandard_big_title_hide != 1 ) || is_customize_preview() ) {

	$tradiestandard_big_title_image = get_theme_mod( 'tradiestandard_big_title_image', get_template_directory_uri() . '/assets/images/slide1.jpg' );
	$tradiestandard_big_title_title = get_theme_mod( 'tradiestandard_big_title_title', 'Rack' );
	$tradiestandard_big_title_subtitle = get_theme_mod( 'tradiestandard_big_title_subtitle', __( 'WooCommerce Theme', 'tradiestandard' ) );
	$tradiestandard_big_title_button_label = get_theme_mod( 'tradiestandard_big_title_button_label', __( 'Read more', 'tradiestandard' ) );
	$tradiestandard_big_title_button_link = get_theme_mod( 'tradiestandard_big_title_button_link', __( '#', 'tradiestandard' ) );

	if ( ! empty( $tradiestandard_big_title_image ) ) {

		echo '<div class="hero-slider">';

		echo '<ul class="slides">';

		echo '<li class="bg-dark" style="background-image:url(' . esc_url( $tradiestandard_big_title_image ) . ')">';

		echo '<div class="home-slider-overlay"></div>';
		echo '<div class="hs-caption">';
		echo '<div class="caption-content">';

		if ( ! empty( $tradiestandard_big_title_title ) ) {
			echo '<div class="hs-title-size-4 font-alt mb-30">' . $tradiestandard_big_title_title . '</div>';
		}

		if ( ! empty( $tradiestandard_big_title_subtitle ) ) {
			echo '<div class="hs-title-size-1 font-alt mb-40">' . $tradiestandard_big_title_subtitle . '</div>';
		}

		if ( ! empty( $tradiestandard_big_title_button_label ) && ! empty( $tradiestandard_big_title_button_link ) ) {
			echo '<a href="' . esc_url( $tradiestandard_big_title_button_link ) . '" class="section-scroll btn btn-border-w btn-round">' . $tradiestandard_big_title_button_label . '</a>';
		}
		echo '</div><!-- .caption-content -->';
		echo '</div><!-- .hs-caption -->';

		echo '</li><!-- .bg-dark -->';

		echo '</ul><!-- .slides -->';

		echo '</div><!-- .hero-slider -->';

	}

	echo '</section >';

} // End if().

