<?php
/**
 * Front page Latest Products Section
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

$tradiestandard_products_hide = get_theme_mod( 'tradiestandard_products_hide', false );
if ( ! empty( $tradiestandard_products_hide ) && (bool) $tradiestandard_products_hide === true ) {
	return;
}

/* Latest products */

echo '<section id="latest" class="module-small">';

echo '<div class="container">';

if ( current_user_can( 'edit_theme_options' ) ) {
	$tradiestandard_products_title = get_theme_mod( 'tradiestandard_products_title', __( 'Latest products', 'tradiestandard' ) );
} else {
	$tradiestandard_products_title = get_theme_mod( 'tradiestandard_products_title' );
}

if ( ! empty( $tradiestandard_products_title ) ) :
	echo '<div class="row">';
	echo '<div class="col-sm-6 col-sm-offset-3">';
	echo '<h2 class="module-title font-alt product-hide-title">' . $tradiestandard_products_title . '</h2>';
	echo '</div>';
	echo '</div>';
endif;

$tradiestandard_products_shortcode = get_theme_mod( 'tradiestandard_products_shortcode' );
$tradiestandard_products_category  = get_theme_mod( 'tradiestandard_products_category' );


$tax_query_item  = array();
$meta_query_item = array();
if ( taxonomy_exists( 'product_visibility' ) ) {
	$tax_query_item = array(
		array(
			'taxonomy' => 'product_visibility',
			'field'    => 'term_id',
			'terms'    => 'exclude-from-catalog',
			'operator' => 'NOT IN',

		),
	);
} else {
	$meta_query_item = array(
		'key'     => '_visibility',
		'value'   => 'hidden',
		'compare' => '!=',
	);

}

$tradiestandard_latest_args = array(
	'post_type'      => 'product',
	'posts_per_page' => 8,
	'orderby'        => 'date',
	'order'          => 'DESC',
);

if ( ! empty( $tradiestandard_products_category ) && $tradiestandard_products_category != '-' ) {
	$tradiestandard_latest_args['tax_query'] = array(
		array(
			'taxonomy' => 'product_cat',
			'field'    => 'term_id',
			'terms'    => $tradiestandard_products_category,
		),
	);
}

if ( ! empty( $tax_query_item ) ) {
	$tradiestandard_latest_args['tax_query']['relation'] = 'AND';
	$tradiestandard_latest_args['tax_query']             = array_merge( $tradiestandard_latest_args['tax_query'], $tax_query_item );
}

if ( ! empty( $meta_query_item ) ) {
	$tradiestandard_latest_args['meta_query'] = $meta_query_item;
}


/* Woocommerce shortcode */
if ( isset( $tradiestandard_products_shortcode ) && ! empty( $tradiestandard_products_shortcode ) ) :
	echo '<div class="products_shortcode">';
	echo do_shortcode( $tradiestandard_products_shortcode );
	echo '</div>';

	/* Products from category */
elseif ( isset( $tradiestandard_products_category ) && ! empty( $tradiestandard_products_category ) && ( $tradiestandard_products_category != '-' ) ) :

	$tradiestandard_latest_loop = new WP_Query( $tradiestandard_latest_args );
	if ( $tradiestandard_latest_loop->have_posts() ) :

		echo '<div class="row multi-columns-row">';

		while ( $tradiestandard_latest_loop->have_posts() ) :

			$tradiestandard_latest_loop->the_post();
			global $product;
			echo '<div class="col-sm-6 col-md-3 col-lg-3">';
			echo '<div class="shop-item">';
			echo '<div class="shop-item-image">';

			$tradiestandard_gallery_attachment_ids = false;
			if ( function_exists( 'method_exists' ) && method_exists( $product, 'get_gallery_image_ids' ) ) {
				$tradiestandard_gallery_attachment_ids = $product->get_gallery_image_ids();
			} elseif ( function_exists( 'method_exists' ) && method_exists( $product, 'get_gallery_attachment_ids' ) ) {
				$tradiestandard_gallery_attachment_ids = $product->get_gallery_attachment_ids();
			}

			if ( has_post_thumbnail( $tradiestandard_latest_loop->post->ID ) ) :
				echo get_the_post_thumbnail( $tradiestandard_latest_loop->post->ID, 'shop_catalog' );

				if ( $tradiestandard_gallery_attachment_ids ) :
					echo wp_get_attachment_image( $tradiestandard_gallery_attachment_ids[0], 'shop_catalog' );
				endif;

			elseif ( $tradiestandard_gallery_attachment_ids ) :

				if ( $tradiestandard_gallery_attachment_ids[0] ) :
					echo wp_get_attachment_image( $tradiestandard_gallery_attachment_ids[0], 'shop_catalog' );
				endif;

				if ( $tradiestandard_gallery_attachment_ids[1] ) :
					echo wp_get_attachment_image( $tradiestandard_gallery_attachment_ids[1], 'shop_catalog' );
				endif;

			else :
				if ( function_exists( 'wc_placeholder_img_src' ) ) :
					echo '<img src="' . esc_url( wc_placeholder_img_src() ) . '" alt="Placeholder" width="65px" height="115px" />';
				endif;
			endif;

			echo '</div>';
			echo '<h4 class="shop-item-title font-alt"><a href="' . esc_url( get_permalink() ) . '">' . get_the_title() . '</a></h4>';
			echo '</div>';
			echo '</div>';

		endwhile;

		echo '</div>';

		echo '<div class="row mt-30">';
		echo '<div class="col-sm-12 align-center">';
		if ( function_exists( 'wc_get_page_id' ) ) {
			echo '<a href="' . esc_url( get_permalink( wc_get_page_id( 'shop' ) ) ) . '" class="btn btn-b btn-round">' . apply_filters( 'tradiestandard_see_all_products_label', __( 'See all products', 'tradiestandard' ) ) . '</a>';
		} elseif ( function_exists( 'woocommerce_get_page_id' ) ) {
			echo '<a href="' . esc_url( get_permalink( woocommerce_get_page_id( 'shop' ) ) ) . '" class="btn btn-b btn-round">' . apply_filters( 'tradiestandard_see_all_products_label', __( 'See all products', 'tradiestandard' ) ) . '</a>';
		}
		echo '</div>';
		echo '</div>';

	else :

		echo '<div class="row">';
		echo '<div class="col-sm-6 col-sm-offset-3">';
		echo '<p class="">' . __( 'No products found.', 'tradiestandard' ) . '</p>';
		echo '</div>';
		echo '</div>';

	endif;

	wp_reset_postdata();

	/* Latest products */
else :

	$tradiestandard_latest_loop = new WP_Query( $tradiestandard_latest_args );

	if ( $tradiestandard_latest_loop->have_posts() ) :

		echo '<div class="row multi-columns-row">';

		while ( $tradiestandard_latest_loop->have_posts() ) :

			$tradiestandard_latest_loop->the_post();
			global $product;

			echo '<div class="col-sm-6 col-md-3 col-lg-3">';
			echo '<div class="shop-item">';
			echo '<div class="shop-item-image">';

			$tradiestandard_gallery_attachment_ids = false;
			if ( function_exists( 'method_exists' ) && method_exists( $product, 'get_gallery_image_ids' ) ) {
				$tradiestandard_gallery_attachment_ids = $product->get_gallery_image_ids();
			} elseif ( function_exists( 'method_exists' ) && method_exists( $product, 'get_gallery_attachment_ids' ) ) {
				$tradiestandard_gallery_attachment_ids = $product->get_gallery_attachment_ids();
			}

			if ( has_post_thumbnail( $tradiestandard_latest_loop->post->ID ) ) :
				echo get_the_post_thumbnail( $tradiestandard_latest_loop->post->ID, 'shop_catalog' );

				if ( $tradiestandard_gallery_attachment_ids ) :
					echo wp_get_attachment_image( $tradiestandard_gallery_attachment_ids[0], 'shop_catalog' );
				endif;

			elseif ( $tradiestandard_gallery_attachment_ids ) :

				if ( $tradiestandard_gallery_attachment_ids[0] ) :
					echo wp_get_attachment_image( $tradiestandard_gallery_attachment_ids[0], 'shop_catalog' );
				endif;

				if ( $tradiestandard_gallery_attachment_ids[1] ) :
					echo wp_get_attachment_image( $tradiestandard_gallery_attachment_ids[1], 'shop_catalog' );
				endif;

			else :
				if ( function_exists( 'wc_placeholder_img_src' ) ) :
					echo '<img src="' . esc_url( wc_placeholder_img_src() ) . '" alt="Placeholder" width="65px" height="115px" />';
				endif;
			endif;

			echo '</div>';
			echo '<h4 class="shop-item-title font-alt"><a href="' . esc_url( get_permalink() ) . '">' . get_the_title() . '</a></h4>';
			echo '</div>';
			echo '</div>';

		endwhile;

		echo '</div>';

		echo '<div class="row mt-30">';
		echo '<div class="col-sm-12 align-center">';
		if ( function_exists( 'wc_get_page_id' ) ) {
			echo '<a href="' . esc_url( get_permalink( wc_get_page_id( 'shop' ) ) ) . '" class="btn btn-b btn-round">' . apply_filters( 'tradiestandard_see_all_products_label', __( 'See all products', 'tradiestandard' ) ) . '</a>';
		} elseif ( function_exists( 'woocommerce_get_page_id' ) ) {
			echo '<a href="' . esc_url( get_permalink( woocommerce_get_page_id( 'shop' ) ) ) . '" class="btn btn-b btn-round">' . apply_filters( 'tradiestandard_see_all_products_label', __( 'See all products', 'tradiestandard' ) ) . '</a>';
		}
		echo '</div>';
		echo '</div>';

	elseif ( current_user_can( 'edit_theme_options' ) ) :
		echo '<div class="row">';
		echo '<div class="col-sm-6 col-sm-offset-3">';
		echo '<p class="">' . __( 'For this section to work, you first need to install the WooCommerce plugin , create some products, and insert a WooCommerce shortocode or select a product category in Customize -> Frontpage sections -> Products section', 'tradiestandard' ) . '</p>';
		echo '</div>';
		echo '</div>';
	endif;

	wp_reset_postdata();

endif;

echo '</div><!-- .container -->';

echo '</section>';




