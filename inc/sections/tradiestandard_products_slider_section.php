<<<<<<< HEAD
<?php
/**
 * Front page Product Slider Section
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

$tradiestandard_products_slider_hide = get_theme_mod( 'tradiestandard_products_slider_hide', false );
if ( ! empty( $tradiestandard_products_slider_hide ) && (bool) $tradiestandard_products_slider_hide === true ) {
	return;
}

echo '<section class="home-product-slider">';


echo '<div class="container">';

if ( current_user_can( 'edit_theme_options' ) ) {
	$tradiestandard_products_slider_title = get_theme_mod( 'tradiestandard_products_slider_title', __( 'Exclusive products', 'tradiestandard' ) );
	if ( ! class_exists( 'WC_Product' ) ) {
		$tradiestandard_products_slider_subtitle = get_theme_mod( 'tradiestandard_products_slider_subtitle', __( 'For this section to work, you first need to install the WooCommerce plugin , create some products, and select a product category in Customize -> Frontpage sections -> Products slider section', 'tradiestandard' ) );
	} else {
		$tradiestandard_products_slider_subtitle = get_theme_mod( 'tradiestandard_products_slider_subtitle' );
	}
} else {
	$tradiestandard_products_slider_title = get_theme_mod( 'tradiestandard_products_slider_title',__( 'Exclusive products', 'tradiestandard' ) );
	$tradiestandard_products_slider_subtitle = get_theme_mod( 'tradiestandard_products_slider_subtitle',__( 'Special category of products', 'tradiestandard' ) );
}


if ( ! empty( $tradiestandard_products_slider_title ) || ! empty( $tradiestandard_products_slider_subtitle ) ) :
	echo '<div class="row">';
	echo '<div class="col-sm-6 col-sm-offset-3">';
	if ( ! empty( $tradiestandard_products_slider_title ) ) :
		echo '<h2 class="module-title font-alt home-prod-title">' . $tradiestandard_products_slider_title . '</h2>';
	endif;
	if ( ! empty( $tradiestandard_products_slider_subtitle ) ) :
		echo '<div class="module-subtitle font-serif home-prod-subtitle">' . $tradiestandard_products_slider_subtitle . '</div>';
	endif;
	echo '</div>';
	echo '</div><!-- .row -->';
endif;

$tradiestandard_products_slider_category = get_theme_mod( 'tradiestandard_products_slider_category' );


$tax_query_item  = array();
$meta_query_item = array();

if ( taxonomy_exists( 'product_visibility' ) ) {
	$tax_query_item = array(
		array(
			'taxonomy' => 'product_visibility',
			'field'    => 'term_id',
			'terms'    => 'exclude-from-catalog',
			'operator' => 'NOT IN',
		),
	);
} else {
	$meta_query_item = array(
		'key'     => '_visibility',
		'value'   => 'hidden',
		'compare' => '!=',
	);
}

$tradiestandard_products_slider_args = array(
	'post_type'      => 'product',
	'posts_per_page' => 10,
);

if ( ! empty( $tradiestandard_products_slider_category ) && ( $tradiestandard_products_slider_category != '-' ) ) {
	$tradiestandard_products_slider_args['tax_query'] = array(
		array(
			'taxonomy' => 'product_cat',
			'field'    => 'term_id',
			'terms'    => $tradiestandard_products_slider_category,
		),
	);
}

if ( ! empty( $tax_query_item ) ) {
	$tradiestandard_products_slider_args['tax_query']['relation'] = 'AND';
	$tradiestandard_products_slider_args['tax_query']             = array_merge( $tradiestandard_products_slider_args['tax_query'], $tax_query_item );
}

if ( ! empty( $meta_query_item ) ) {
	$tradiestandard_products_slider_args['meta_query'] = $meta_query_item;
}



if ( ! empty( $tradiestandard_products_slider_category ) && ( $tradiestandard_products_slider_category != '-' ) ) :

	$tradiestandard_products_slider_loop = new WP_Query( $tradiestandard_products_slider_args );

	if ( $tradiestandard_products_slider_loop->have_posts() ) :

		$rtl_slider = apply_filters( 'tradiestandard_products_slider_section_rtl', 'false' );
		$number_of_items = apply_filters( 'tradiestandard_products_slider_section_items', 5 );
		$pagination = apply_filters( 'tradiestandard_products_slider_section_pagination', 'false' );
		$navigation = apply_filters( 'tradiestandard_products_slider_section_navigation', 'false' );

		echo '<div class="row">';

		echo '<div id="prod_slider" class="owl-carousel text-center" data-items="' . esc_attr( $number_of_items ) . '" data-pagination="' . esc_attr( $pagination ) . '" data-navigation="' . esc_attr( $navigation ) . '" data-rtl="' . esc_attr( $rtl_slider ) . '" >';

		while ( $tradiestandard_products_slider_loop->have_posts() ) :

			$tradiestandard_products_slider_loop->the_post();
			global $product;
			echo '<div class="owl-item">';
			echo '<div class="col-sm-12">';
			echo '<div class="ex-product">';
			if ( function_exists( 'woocommerce_get_product_thumbnail' ) ) :
				echo '<a href="' . esc_url( get_permalink() ) . '">' . woocommerce_get_product_thumbnail() . '</a>';
			endif;
			echo '<h4 class="shop-item-title font-alt"><a href="' . esc_url( get_permalink() ) . '">' . get_the_title() . '</a></h4>';
			echo '</div>';
			echo '</div>';
			echo '</div>';

		endwhile;

		wp_reset_postdata();
		echo '</div>';

		echo '</div>';

	endif;

else :

	$tradiestandard_products_slider_loop = new WP_Query( $tradiestandard_products_slider_args );

	if ( $tradiestandard_products_slider_loop->have_posts() ) :

		$rtl_slider = apply_filters( 'tradiestandard_products_slider_section_rtl', 'false' );
		$number_of_items = apply_filters( 'tradiestandard_products_slider_section_items', 5 );
		$pagination = apply_filters( 'tradiestandard_products_slider_section_pagination', 'false' );
		$navigation = apply_filters( 'tradiestandard_products_slider_section_navigation', 'false' );
		echo '<div class="row">';

		echo '<div class="owl-carousel text-center" data-items="' . esc_attr( $number_of_items ) . '" data-pagination="' . esc_attr( $pagination ) . '" data-navigation="' . esc_attr( $navigation ) . '" data-rtl="' . esc_attr( $rtl_slider ) . '">';

		while ( $tradiestandard_products_slider_loop->have_posts() ) :

			$tradiestandard_products_slider_loop->the_post();
			global $product;
			echo '<div class="owl-item">';
			echo '<div class="col-sm-12">';
			echo '<div class="ex-product">';
			if ( function_exists( 'woocommerce_get_product_thumbnail' ) ) :
				echo '<a href="' . esc_url( get_permalink() ) . '">' . woocommerce_get_product_thumbnail() . '</a>';
			endif;
			echo '<h4 class="shop-item-title font-alt"><a href="' . esc_url( get_permalink() ) . '">' . get_the_title() . '</a></h4>';

			echo '</div>';
			echo '</div>';
			echo '</div>';

		endwhile;

		wp_reset_postdata();
		echo '</div>';

		echo '</div>';

	endif;

endif;

echo '</div>';

echo '</section>';




=======
<?php
/**
 * Front page Product Slider Section
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

$tradiestandard_products_slider_hide = get_theme_mod( 'tradiestandard_products_slider_hide', false );
if ( ! empty( $tradiestandard_products_slider_hide ) && (bool) $tradiestandard_products_slider_hide === true ) {
	return;
}

echo '<section class="home-product-slider">';


echo '<div class="container">';

if ( current_user_can( 'edit_theme_options' ) ) {
	$tradiestandard_products_slider_title = get_theme_mod( 'tradiestandard_products_slider_title', __( 'Exclusive products', 'tradiestandard' ) );
	if ( ! class_exists( 'WC_Product' ) ) {
		$tradiestandard_products_slider_subtitle = get_theme_mod( 'tradiestandard_products_slider_subtitle', __( 'For this section to work, you first need to install the WooCommerce plugin , create some products, and select a product category in Customize -> Frontpage sections -> Products slider section', 'tradiestandard' ) );
	} else {
		$tradiestandard_products_slider_subtitle = get_theme_mod( 'tradiestandard_products_slider_subtitle' );
	}
} else {
	$tradiestandard_products_slider_title = get_theme_mod( 'tradiestandard_products_slider_title',__( 'Exclusive products', 'tradiestandard' ) );
	$tradiestandard_products_slider_subtitle = get_theme_mod( 'tradiestandard_products_slider_subtitle',__( 'Special category of products', 'tradiestandard' ) );
}


if ( ! empty( $tradiestandard_products_slider_title ) || ! empty( $tradiestandard_products_slider_subtitle ) ) :
	echo '<div class="row">';
	echo '<div class="col-sm-6 col-sm-offset-3">';
	if ( ! empty( $tradiestandard_products_slider_title ) ) :
		echo '<h2 class="module-title font-alt home-prod-title">' . $tradiestandard_products_slider_title . '</h2>';
	endif;
	if ( ! empty( $tradiestandard_products_slider_subtitle ) ) :
		echo '<div class="module-subtitle font-serif home-prod-subtitle">' . $tradiestandard_products_slider_subtitle . '</div>';
	endif;
	echo '</div>';
	echo '</div><!-- .row -->';
endif;

$tradiestandard_products_slider_category = get_theme_mod( 'tradiestandard_products_slider_category' );


$tax_query_item  = array();
$meta_query_item = array();

if ( taxonomy_exists( 'product_visibility' ) ) {
	$tax_query_item = array(
		array(
			'taxonomy' => 'product_visibility',
			'field'    => 'term_id',
			'terms'    => 'exclude-from-catalog',
			'operator' => 'NOT IN',
		),
	);
} else {
	$meta_query_item = array(
		'key'     => '_visibility',
		'value'   => 'hidden',
		'compare' => '!=',
	);
}

$tradiestandard_products_slider_args = array(
	'post_type'      => 'product',
	'posts_per_page' => 10,
);

if ( ! empty( $tradiestandard_products_slider_category ) && ( $tradiestandard_products_slider_category != '-' ) ) {
	$tradiestandard_products_slider_args['tax_query'] = array(
		array(
			'taxonomy' => 'product_cat',
			'field'    => 'term_id',
			'terms'    => $tradiestandard_products_slider_category,
		),
	);
}

if ( ! empty( $tax_query_item ) ) {
	$tradiestandard_products_slider_args['tax_query']['relation'] = 'AND';
	$tradiestandard_products_slider_args['tax_query']             = array_merge( $tradiestandard_products_slider_args['tax_query'], $tax_query_item );
}

if ( ! empty( $meta_query_item ) ) {
	$tradiestandard_products_slider_args['meta_query'] = $meta_query_item;
}



if ( ! empty( $tradiestandard_products_slider_category ) && ( $tradiestandard_products_slider_category != '-' ) ) :

	$tradiestandard_products_slider_loop = new WP_Query( $tradiestandard_products_slider_args );

	if ( $tradiestandard_products_slider_loop->have_posts() ) :

		$rtl_slider = apply_filters( 'tradiestandard_products_slider_section_rtl', 'false' );
		$number_of_items = apply_filters( 'tradiestandard_products_slider_section_items', 5 );
		$pagination = apply_filters( 'tradiestandard_products_slider_section_pagination', 'false' );
		$navigation = apply_filters( 'tradiestandard_products_slider_section_navigation', 'false' );

		echo '<div class="row">';

		echo '<div id="prod_slider" class="owl-carousel text-center" data-items="' . esc_attr( $number_of_items ) . '" data-pagination="' . esc_attr( $pagination ) . '" data-navigation="' . esc_attr( $navigation ) . '" data-rtl="' . esc_attr( $rtl_slider ) . '" >';

		while ( $tradiestandard_products_slider_loop->have_posts() ) :

			$tradiestandard_products_slider_loop->the_post();
			global $product;
			echo '<div class="owl-item">';
			echo '<div class="col-sm-12">';
			echo '<div class="ex-product">';
			if ( function_exists( 'woocommerce_get_product_thumbnail' ) ) :
				echo '<a href="' . esc_url( get_permalink() ) . '">' . woocommerce_get_product_thumbnail() . '</a>';
			endif;
			echo '<h4 class="shop-item-title font-alt"><a href="' . esc_url( get_permalink() ) . '">' . get_the_title() . '</a></h4>';
			echo '</div>';
			echo '</div>';
			echo '</div>';

		endwhile;

		wp_reset_postdata();
		echo '</div>';

		echo '</div>';

	endif;

else :

	$tradiestandard_products_slider_loop = new WP_Query( $tradiestandard_products_slider_args );

	if ( $tradiestandard_products_slider_loop->have_posts() ) :

		$rtl_slider = apply_filters( 'tradiestandard_products_slider_section_rtl', 'false' );
		$number_of_items = apply_filters( 'tradiestandard_products_slider_section_items', 5 );
		$pagination = apply_filters( 'tradiestandard_products_slider_section_pagination', 'false' );
		$navigation = apply_filters( 'tradiestandard_products_slider_section_navigation', 'false' );
		echo '<div class="row">';

		echo '<div class="owl-carousel text-center" data-items="' . esc_attr( $number_of_items ) . '" data-pagination="' . esc_attr( $pagination ) . '" data-navigation="' . esc_attr( $navigation ) . '" data-rtl="' . esc_attr( $rtl_slider ) . '">';

		while ( $tradiestandard_products_slider_loop->have_posts() ) :

			$tradiestandard_products_slider_loop->the_post();
			global $product;
			echo '<div class="owl-item">';
			echo '<div class="col-sm-12">';
			echo '<div class="ex-product">';
			if ( function_exists( 'woocommerce_get_product_thumbnail' ) ) :
				echo '<a href="' . esc_url( get_permalink() ) . '">' . woocommerce_get_product_thumbnail() . '</a>';
			endif;
			echo '<h4 class="shop-item-title font-alt"><a href="' . esc_url( get_permalink() ) . '">' . get_the_title() . '</a></h4>';

			echo '</div>';
			echo '</div>';
			echo '</div>';

		endwhile;

		wp_reset_postdata();
		echo '</div>';

		echo '</div>';

	endif;

endif;

echo '</div>';

echo '</section>';




>>>>>>> 2f60927d06dd186fefafb15442a32abd421ec7aa
