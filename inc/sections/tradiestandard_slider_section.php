<?php
/**
 * Front page Slider Section
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

$tradiestandard_slider_hide = get_theme_mod( 'tradiestandard_slider_hide', false );
if ( ! empty( $tradiestandard_slider_hide ) && (bool) $tradiestandard_slider_hide === true ) {
	return;
}
// $tradiestandard_homepage_slider_shortcode = get_theme_mod( 'tradiestandard_homepage_slider_shortcode' );

echo '<section id="home" class="home-section home-fade">';

// if ( ! empty( $tradiestandard_homepage_slider_shortcode ) ) {
// 	echo do_shortcode( $tradiestandard_homepage_slider_shortcode );
// } else {

	$tradiestandard_slider = get_theme_mod( 'tradiestandard_slider', json_encode( array(
		array(
			'image_url' => get_template_directory_uri() . '/assets/images/slide1.jpg',
			'link'      => '#',
			'text'      => __( 'Rack', 'tradiestandard' ),
			'subtext'   => __( 'WooCommerce Theme', 'tradiestandard' ),
			'label'     => __( 'Read more', 'tradiestandard' ),
		),
		array(
			'image_url' => get_template_directory_uri() . '/assets/images/slide2.jpg',
			'link'      => '#',
			'text'      => __( 'Rack', 'tradiestandard' ),
			'subtext'   => __( 'WooCommerce Theme', 'tradiestandard' ),
			'label'     => __( 'Read more', 'tradiestandard' ),
		),
		array(
			'image_url' => get_template_directory_uri() . '/assets/images/slide3.jpg',
			'link'      => '#',
			'text'      => __( 'Rack', 'tradiestandard' ),
			'subtext'   => __( 'WooCommerce Theme', 'tradiestandard' ),
			'label'     => __( 'Read more', 'tradiestandard' ),
		),
	) ) );

	if ( ! empty( $tradiestandard_slider ) ) {

		$tradiestandard_slider_decoded = json_decode( $tradiestandard_slider );

		if ( ! empty( $tradiestandard_slider_decoded ) ) {

			echo '<div class="hero-slider">';

			echo '<ul class="slides">';

			foreach ( $tradiestandard_slider_decoded as $tradiestandard_slide ) {

				if ( ! empty( $tradiestandard_slide->image_url ) ) {

					echo '<li class="bg-dark-30 bg-dark">';
					echo '<img src="' . esc_url( $tradiestandard_slide->image_url ) . '">';

					echo '<div class="hs-caption">';
					echo '<div class="caption-content">';

					if ( ! empty( $tradiestandard_slide->text ) ) {
						echo '<div class="hs-title-size-4 font-alt mb-30">' . $tradiestandard_slide->text . '</div>';
					}

					if ( ! empty( $tradiestandard_slide->subtext ) ) {
						echo '<div class="hs-title-size-1 font-alt mb-40">' . $tradiestandard_slide->subtext . '</div>';
					}

					if ( ! empty( $tradiestandard_slide->link ) && ! empty( $tradiestandard_slide->label ) ) {
						echo '<a href="' . esc_url( $tradiestandard_slide->link ) . '" class="section-scroll btn btn-border-w btn-round">' . $tradiestandard_slide->label . '</a>';
					}
					echo '</div>';
					echo '</div>';
					echo '</li>';

				}// End if().
			}// End foreach().

			echo '</ul>';

			echo '</div>';

		}// End if().
	}// End if().
// }// End if().

echo '</section >';



