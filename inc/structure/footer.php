<?php
/**
 * Template functions used for the site footer.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

if ( ! function_exists( 'tradiestandard_footer_widgets' ) ) {
	/**
	 * Display the footer widgets
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function tradiestandard_footer_widgets() {
		?>
		<!-- Widgets start -->

	<?php if ( is_active_sidebar( 'sidebar-footer-area-1' ) || is_active_sidebar( 'sidebar-footer-area-2' ) || is_active_sidebar( 'sidebar-footer-area-3' ) || is_active_sidebar( 'sidebar-footer-area-4' ) ) : ?>

		<div class="module-small bg-dark tradiestandard_footer_sidebar">
			<div class="container">
				<div class="row">

					<?php if ( is_active_sidebar( 'sidebar-footer-area-1' ) ) : ?>
						<div class="col-sm-6 col-md-3 footer-sidebar-wrap">
							<?php dynamic_sidebar( 'sidebar-footer-area-1' ); ?>
						</div>
					<?php endif; ?>
					<!-- Widgets end -->

					<?php if ( is_active_sidebar( 'sidebar-footer-area-2' ) ) : ?>
						<div class="col-sm-6 col-md-3 footer-sidebar-wrap">
							<?php dynamic_sidebar( 'sidebar-footer-area-2' ); ?>
						</div>
					<?php endif; ?>
					<!-- Widgets end -->

					<?php if ( is_active_sidebar( 'sidebar-footer-area-3' ) ) : ?>
						<div class="col-sm-6 col-md-3 footer-sidebar-wrap">
							<?php dynamic_sidebar( 'sidebar-footer-area-3' ); ?>
						</div>
					<?php endif; ?>
					<!-- Widgets end -->


					<?php if ( is_active_sidebar( 'sidebar-footer-area-4' ) ) : ?>
						<div class="col-sm-6 col-md-3 footer-sidebar-wrap">
							<?php dynamic_sidebar( 'sidebar-footer-area-4' ); ?>
						</div>
					<?php endif; ?>
					<!-- Widgets end -->

				</div><!-- .row -->
			</div>
		</div>

	<?php endif; ?>

		<?php
	}
}// End if().

if ( ! function_exists( 'tradiestandard_footer_copyright_and_socials' ) ) {
	/**
	 * Display the theme copyright and socials
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function tradiestandard_footer_copyright_and_socials() {

		?>
		<!-- Footer start -->
		<footer class="footer bg-dark">
			<!-- Divider -->
			<hr class="divider-d">
			<!-- Divider -->
			<div class="container">

				<div class="row">

					<?php
					/* Copyright */
					$tradiestandard_copyright = apply_filters( 'tradiestandard_footer_copyright_filter', get_theme_mod( 'tradiestandard_copyright' ) );
					echo '<div class="col-sm-6">';
					if ( ! empty( $tradiestandard_copyright ) ) :
						echo '<p class="copyright font-alt">' . $tradiestandard_copyright . '</p>';
						endif;

						$tradiestandard_site_info_hide = apply_filters( 'tradiestandard_footer_socials_filter', get_theme_mod( 'tradiestandard_site_info_hide' ) );
					if ( isset( $tradiestandard_site_info_hide ) && $tradiestandard_site_info_hide != 1 ) {
						echo apply_filters( 'tradiestandard_site_info','<p class="tradiestandard-poweredby-box">Rask ' . __( 'powered by','tradiestandard' ) . '<a class="tradiestandard-poweredby" href="http://wordpress.org/" rel="nofollow"> WordPress</a></p>' );
					}
					echo '</div>';

					/* Socials icons */

					$tradiestandard_socials = get_theme_mod( 'tradiestandard_socials' );

					if ( ! empty( $tradiestandard_socials ) ) :

						$tradiestandard_socials_decoded = json_decode( $tradiestandard_socials );

						if ( ! empty( $tradiestandard_socials_decoded ) ) :

							echo '<div class="col-sm-6">';

								echo '<div class="footer-social-links">';

							foreach ( $tradiestandard_socials_decoded as $tradiestandard_social ) :

								if ( ! empty( $tradiestandard_social->icon_value ) && ! empty( $tradiestandard_social->link ) ) {

									echo '<a href="' . esc_url( $tradiestandard_social->link ) . '"><span class="' . esc_attr( $tradiestandard_social->icon_value ) . '"></span></a>';
								}

									endforeach;

								echo '</div>';

							echo '</div>';

						endif;

					endif;
					?>
				</div><!-- .row -->

			</div>
		</footer>
		<!-- Footer end -->
		<?php
	}
}// End if().


if ( ! function_exists( 'tradiestandard_footer_wrap_open' ) ) {
	/**
	 * Display the theme copyright and socials
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function tradiestandard_footer_wrap_open() {
		echo '</div><div class="bottom-page-wrap">';
	}
}


if ( ! function_exists( 'tradiestandard_footer_wrap_close' ) ) {
	/**
	 * Display the theme copyright and socials
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function tradiestandard_footer_wrap_close() {
		echo '</div><!-- .bottom-page-wrap -->';
	}
}
