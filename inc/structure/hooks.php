<?php
/**
 * tradiestandard Hooks
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

/**
 * General
 *
 * @see  tradiestandard_setup()
 * @see  tradiestandard_widgets_init()
 * @see  tradiestandard_scripts()
 * @see  tradiestandard_get_sidebar()
 */
add_action( 'after_setup_theme',				'tradiestandard_setup' );
add_action( 'widgets_init',						'tradiestandard_widgets_init' );
add_action( 'wp_enqueue_scripts',				'tradiestandard_scripts',					10 );
add_action( 'admin_enqueue_scripts',        	'tradiestandard_admin_styles',           	10 );
add_action( 'tradiestandard_sidebar',				'tradiestandard_get_sidebar',				10 );
add_action( 'tradiestandard_sidebar_shop_archive',	'tradiestandard_get_sidebar_shop_archive',	10 );


/**
 * Header
 *
 * @see  tradiestandard_primary_navigation()
 */
add_action( 'tradiestandard_header', 'tradiestandard_primary_navigation',		50 );

/**
 * Footer
 *
 * @see  tradiestandard_footer_widgets()
 * @see  tradiestandard_footer_copyright_and_socials()
 */
add_action( 'tradiestandard_footer', 'tradiestandard_footer_wrap_open',	             	5 );
add_action( 'tradiestandard_footer', 'tradiestandard_footer_widgets',	                    10 );
add_action( 'tradiestandard_footer', 'tradiestandard_footer_copyright_and_socials',	    20 );
add_action( 'tradiestandard_footer', 'tradiestandard_footer_wrap_close',	             	30 );

/**
 * Homepage
 *
 * @see  tradiestandard_homepage_content()
 * @see  tradiestandard_product_categories()
 * @see  tradiestandard_recent_products()
 * @see  tradiestandard_featured_products()
 * @see  tradiestandard_popular_products()
 * @see  tradiestandard_on_sale_products()
 */
add_action( 'homepage', 'tradiestandard_homepage_content',		10 );
add_action( 'homepage', 'tradiestandard_product_categories',	    20 );
add_action( 'homepage', 'tradiestandard_recent_products',		30 );
add_action( 'homepage', 'tradiestandard_featured_products',		40 );
add_action( 'homepage', 'tradiestandard_popular_products',		50 );
add_action( 'homepage', 'tradiestandard_on_sale_products',		60 );

/**
 * Posts
 *
 * @see  tradiestandard_post_header()
 * @see  tradiestandard_post_meta()
 * @see  tradiestandard_post_content()
 * @see  tradiestandard_paging_nav()
 * @see  tradiestandard_post_nav()
 * @see  tradiestandard_display_comments()
 */
add_action( 'tradiestandard_loop_post',			'tradiestandard_post_header',		10 );
add_action( 'tradiestandard_loop_post',			'tradiestandard_post_meta',			20 );
add_action( 'tradiestandard_loop_post',			'tradiestandard_post_content',		30 );
add_action( 'tradiestandard_loop_after',		    'tradiestandard_paging_nav',		    10 );
add_action( 'tradiestandard_single_post',		'tradiestandard_post_header',		10 );
add_action( 'tradiestandard_single_post',		'tradiestandard_post_meta',			20 );
add_action( 'tradiestandard_single_post',		'tradiestandard_post_content',		30 );
add_action( 'tradiestandard_single_post_after',	'tradiestandard_post_nav',			10 );
add_action( 'tradiestandard_single_post_after',	'tradiestandard_display_comments',	10 );

/**
 * Pages
 *
 * @see  tradiestandard_page_content()
 * @see  tradiestandard_display_comments()
 */
add_action( 'tradiestandard_page', 			'tradiestandard_page_content',		20 );
add_action( 'tradiestandard_page_after', 	'tradiestandard_display_comments',	10 );

/**
 * Extras
 *
 * @see  tradiestandard_body_classes()
 * @see  tradiestandard_page_menu_args()
 */
add_filter( 'body_class',			'tradiestandard_body_classes' );
add_filter( 'wp_page_menu_args',	'tradiestandard_page_menu_args' );

/**
 * Customize
 *
 * @see  tradiestandard_customize_preview_js()
 * @see  tradiestandard_customize_register()
 * @see  tradiestandard_customizer_script()
 */
	add_action( 'customize_preview_init',               'tradiestandard_customize_preview_js' );
	add_action( 'customize_register',                   'tradiestandard_customize_register' );
	add_action( 'customize_controls_enqueue_scripts',   'tradiestandard_customizer_script' );


/**
 * Shop page
 */
add_action( 'tradiestandard_before_shop', 		'tradiestandard_woocommerce_breadcrumb',	        10 );
add_action( 'tradiestandard_before_shop', 		'woocommerce_catalog_ordering',				20 );


/**
 * Define image sizes
 */
function tradiestandard_woocommerce_image_dimensions() {
	global $pagenow;

	if ( ! isset( $_GET['activated'] ) || $pagenow != 'themes.php' ) {
		return;
	}
		$catalog = array(
		'width' 	=> '262',	// px
		'height'	=> '325',	// px
		'crop'		=> 1,
	);
	$single = array(
		'width' 	=> '555',	// px
		'height'	=> '688',	// px
		'crop'		=> 1,
	);
	$thumbnail = array(
		'width' 	=> '83',	// px
		'height'	=> '103',	// px
		'crop'		=> 1,
	);
	// Image sizes
	update_option( 'shop_catalog_image_size', $catalog ); 		// Product category thumbs
	update_option( 'shop_single_image_size', $single ); 		// Single product image
	update_option( 'shop_thumbnail_image_size', $thumbnail ); 	// Image gallery thumbs
}
add_action( 'after_switch_theme', 'tradiestandard_woocommerce_image_dimensions', 1 );

/**
 * Number of thumbnails per row in product galleries
 *
 * @return int
 */
function tradiestandard_thumb_cols() {
	return 6;
}
add_filter( 'woocommerce_product_thumbnails_columns', 'tradiestandard_thumb_cols', 99 );

