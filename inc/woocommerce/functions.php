<<<<<<< HEAD
<?php
/**
 * General functions used to integrate this theme with WooCommerce.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */


if ( ! function_exists( 'tradiestandard_before_content' ) ) {
	/**
	 * Before Content
	 * Wraps all WooCommerce content in wrappers which match the theme markup
	 *
	 * @since   1.0.0
	 * @return  void
	 */
	function tradiestandard_before_content() {
		?>
		<div class="main">
			<?php
	}
}

if ( ! function_exists( 'tradiestandard_after_content' ) ) {
	/**
	 * After Content
	 * Closes the wrapping divs
	 *
	 * @since   1.0.0
	 * @return  void
	 */
	function tradiestandard_after_content() {
		?>
		</div><!-- .main -->

		<?php
	}
}

if ( ! function_exists( 'tradiestandard_shop_page_wrapper' ) ) {

	/**
	 * Before Shop loop
	 *
	 * @since   1.0.0
	 * @return  void
	 */
	function tradiestandard_shop_page_wrapper() {
		?>
		<section class="module-small module-small-shop">
				<div class="container">

				<?php if ( is_shop() || is_product_tag() || is_product_category() ) :

						do_action( 'tradiestandard_before_shop' );

					if ( is_active_sidebar( 'tradiestandard-sidebar-shop-archive' ) ) : ?>

							<div class="col-sm-9 shop-with-sidebar" id="tradiestandard-blog-container">

						<?php endif; ?>

				<?php endif; ?>

		<?php
	}
}

/**
 * Before Product content
 *
 * @since   1.0.0
 * @return  void
 */
function tradiestandard_product_page_wrapper() {
	echo '<section class="module module-super-small">
			<div class="container product-main-content">';
}

if ( ! function_exists( 'tradiestandard_product_page_wrapper_end' ) ) {
	/**
	 * After Product content
	 * Closes the wrapping div and section
	 *
	 * @since   1.0.0
	 * @return  void
	 */
	function tradiestandard_product_page_wrapper_end() {
		?>
			</div><!-- .container -->
		</section><!-- .module-small -->
			<?php
	}
}

if ( ! function_exists( 'tradiestandard_shop_page_wrapper_end' ) ) {
	/**
	 * After Shop loop
	 * Closes the wrapping div and section
	 *
	 * @since   1.0.0
	 * @return  void
	 */
	function tradiestandard_shop_page_wrapper_end() {
		?>

			<?php if ( (is_shop() || is_product_category() || is_product_tag() ) && is_active_sidebar( 'tradiestandard-sidebar-shop-archive' ) ) :  ?>

				</div>

				<!-- Sidebar column start -->
				<div class="col-sm-3 col-md-3 sidebar sidebar-shop">
					<?php do_action( 'tradiestandard_sidebar_shop_archive' ); ?>
				</div>
				<!-- Sidebar column end -->

			<?php endif; ?>

			</div><!-- .container -->
		</section><!-- .module-small -->
		<?php
	}
}

/**
 * Default loop columns on product archives
 *
 * @return integer products per row
 * @since  1.0.0
 */
function tradiestandard_loop_columns() {
	if ( is_active_sidebar( 'tradiestandard-sidebar-shop-archive' ) ) {
		return apply_filters( 'tradiestandard_loop_columns', 3 ); // 3 products per row
	} else {
		return apply_filters( 'tradiestandard_loop_columns', 4 ); // 4 products per row
	}
}

/**
 * Add 'woocommerce-active' class to the body tag
 *
 * @param  array $classes body classes.
 *
 * @return array $classes modified to include 'woocommerce-active' class.
 */
function tradiestandard_woocommerce_body_class( $classes ) {
	if ( is_woocommerce_activated() ) {
		$classes[] = 'woocommerce-active';
	}

	return $classes;
}

/**
 * WooCommerce specific scripts & stylesheets
 *
 * @since 1.0.0
 */
function tradiestandard_woocommerce_scripts() {

	wp_enqueue_style( 'tradiestandard-woocommerce-style1', get_template_directory_uri() . '/inc/woocommerce/css/woocommerce.css', array(), 'v3' );
}

/**
 * Related Products Args
 *
 * @param  array $args related products args.
 *
 * @since 1.0.0
 * @return  array $args related products args.
 */
function tradiestandard_related_products_args( $args ) {
	$args = apply_filters( 'tradiestandard_related_products_args', array(
		'posts_per_page' => 4,
		'columns'        => 4,
	) );

	return $args;
}

/**
 * Product gallery thumnail columns
 *
 * @return integer number of columns
 * @since  1.0.0
 */
function tradiestandard_thumbnail_columns() {
	return intval( apply_filters( 'tradiestandard_product_thumbnail_columns', 4 ) );
}

/**
 * Products per page
 *
 * @return integer number of products
 * @since  1.0.0
 */
function tradiestandard_products_per_page() {
	return intval( apply_filters( 'tradiestandard_products_per_page', 12 ) );
}

/**
 * Header for shop page
 *
 * @since  1.0.0
 */
function tradiestandard_header_shop_page( $page_title ) {

	$tradiestandard_title = '';

	$tradiestandard_header_image = get_header_image();
	if ( ! empty( $tradiestandard_header_image ) ) :
		$tradiestandard_title = '<section class="' . ( is_woocommerce() ? 'woocommerce-page-title ' : '' ) . 'page-header-module module bg-dark" data-background="' . $tradiestandard_header_image . '">';
	else :
		$tradiestandard_title = '<section class="page-header-module module bg-dark">';
	endif;

		$tradiestandard_title .= '<div class="container">';

			$tradiestandard_title .= '<div class="row">';

				$tradiestandard_title .= '<div class="col-sm-6 col-sm-offset-3">';

	if ( ! empty( $page_title ) ) :

		$tradiestandard_title .= '<h1 class="module-title font-alt">' . $page_title . '</h1>';

					endif;

				$tradiestandard_title .= '</div>';

			$tradiestandard_title .= '</div><!-- .row -->';

		$tradiestandard_title .= '</div>';
	$tradiestandard_title .= '</section>';

	return $tradiestandard_title;
}

/**
 * Products slider on single page product
 *
 * @since  1.0.0
 */
function tradiestandard_products_slider_on_single_page() {

	$tradiestandard_products_slider_single_hide = get_theme_mod( 'tradiestandard_products_slider_single_hide' );

	if ( isset( $tradiestandard_products_slider_single_hide ) && $tradiestandard_products_slider_single_hide != 1 ) :
		echo '<hr class="divider-w">';
		echo '<section class="module module-small-bottom aya">';
	elseif ( is_customize_preview() ) :
		echo '<hr class="divider-w">';
		echo '<section class="module module-small-bottom tradiestandard_hidden_if_not_customizer">';
	endif;

	if ( ( isset( $tradiestandard_products_slider_single_hide ) && $tradiestandard_products_slider_single_hide != 1 ) || is_customize_preview() ) :

			echo '<div class="container">';

				$tradiestandard_products_slider_title = get_theme_mod( 'tradiestandard_products_slider_title',__( 'Exclusive products', 'tradiestandard' ) );
				$tradiestandard_products_slider_subtitle = get_theme_mod( 'tradiestandard_products_slider_subtitle',__( 'Special category of products', 'tradiestandard' ) );

		if ( ! empty( $tradiestandard_products_slider_title ) || ! empty( $tradiestandard_products_slider_subtitle ) ) :
			echo '<div class="row">';
			echo '<div class="col-sm-6 col-sm-offset-3">';
			if ( ! empty( $tradiestandard_products_slider_title ) ) :
				echo '<h2 class="module-title font-alt">' . $tradiestandard_products_slider_title . '</h2>';
			endif;
			if ( ! empty( $tradiestandard_products_slider_subtitle ) ) :
				echo '<div class="module-subtitle font-serif">' . $tradiestandard_products_slider_subtitle . '</div>';
			endif;
			echo '</div>';
			echo '</div><!-- .row -->';
				endif;

				$tradiestandard_products_slider_category = get_theme_mod( 'tradiestandard_products_slider_category' );

		$tax_query_item = array();
		$meta_query_item = array();
		if ( taxonomy_exists( 'product_visibility' ) ) {
			$tax_query_item = array(
				array(
					'taxonomy' => 'product_visibility',
					'field'    => 'term_id',
					'terms'    => 'exclude-from-catalog',
					'operator' => 'NOT IN',
				),
			);
		} else {
			$meta_query_item = array(
				'key'     => '_visibility',
				'value'   => 'hidden',
				'compare' => '!=',
			);
		}

		$tradiestandard_products_slider_args = array(
				'post_type' => 'product',
				'posts_per_page' => 10,
		);

		if ( ! empty( $tradiestandard_products_slider_category ) && ($tradiestandard_products_slider_category != '-') ) {
			$tradiestandard_products_slider_args['tax_query'] = array(
				array(
					'taxonomy'   => 'product_cat',
					'field'      => 'term_id',
					'terms'      => $tradiestandard_products_slider_category,
				),
			);
		}

		if ( ! empty( $tax_query_item ) ) {
			$tradiestandard_products_slider_args['tax_query']['relation'] = 'AND';
			$tradiestandard_products_slider_args['tax_query'] = array_merge( $tradiestandard_products_slider_args['tax_query'],$tax_query_item );
		}

		if ( ! empty( $meta_query_item ) ) {
			$tradiestandard_products_slider_args['meta_query'] = $meta_query_item;
		}

		if ( ! empty( $tradiestandard_products_slider_category ) && ($tradiestandard_products_slider_category != '-') ) :

			$tradiestandard_products_slider_loop = new WP_Query( $tradiestandard_products_slider_args );

			if ( $tradiestandard_products_slider_loop->have_posts() ) :

				$rtl_slider = apply_filters( 'tradiestandard_products_slider_single_rtl', 'false' );
				$number_of_items = apply_filters( 'tradiestandard_products_slider_single_items', 5 );
				$pagination = apply_filters( 'tradiestandard_products_slider_single_pagination', 'false' );
				$navigation = apply_filters( 'tradiestandard_products_slider_single_navigation', 'false' );

				echo '<div class="row">';

				echo '<div id="prod_slider" class="owl-carousel text-center" data-items="' . esc_attr( $number_of_items ) . '" data-pagination="' . esc_attr( $pagination ) . '" data-navigation="' . esc_attr( $navigation ) . '" data-rtl="' . esc_attr( $rtl_slider ) . '" >';

				while ( $tradiestandard_products_slider_loop->have_posts() ) :

					$tradiestandard_products_slider_loop->the_post();

					echo '<div class="owl-item">';
					echo '<div class="col-sm-12">';
					echo '<div class="ex-product">';
					echo '<a href="' . esc_url( get_permalink() ) . '">' . woocommerce_get_product_thumbnail() . '</a>';
					echo '<h4 class="shop-item-title font-alt"><a href="' . esc_url( get_permalink() ) . '">' . get_the_title() . '</a></h4>';
					$product = new WC_Product( get_the_ID() );

					echo '</div>';
					echo '</div>';
					echo '</div>';

					endwhile;

					wp_reset_postdata();
				echo '</div>';

					echo '</div>';

			endif;

				else :

					$tradiestandard_products_slider_loop = new WP_Query( $tradiestandard_products_slider_args );

					if ( $tradiestandard_products_slider_loop->have_posts() ) :

						$rtl_slider = apply_filters( 'tradiestandard_products_slider_single_rtl', 'false' );
						$number_of_items = apply_filters( 'tradiestandard_products_slider_single_items', 5 );
						$pagination = apply_filters( 'tradiestandard_products_slider_single_pagination', 'false' );
						$navigation = apply_filters( 'tradiestandard_products_slider_single_navigation', 'false' );

						echo '<div class="row">';

						echo '<div class="owl-carousel text-center" data-items="' . esc_attr( $number_of_items ) . '" data-pagination="' . esc_attr( $pagination ) . '" data-navigation="' . esc_attr( $navigation ) . '" data-rtl="' . esc_attr( $rtl_slider ) . '" >';

						while ( $tradiestandard_products_slider_loop->have_posts() ) :

							$tradiestandard_products_slider_loop->the_post();

							echo '<div class="owl-item">';
							echo '<div class="col-sm-12">';
							echo '<div class="ex-product">';
							echo '<a href="' . esc_url( get_permalink() ) . '">' . woocommerce_get_product_thumbnail() . '</a>';
							echo '<h4 class="shop-item-title font-alt"><a href="' . esc_url( get_permalink() ) . '">' . get_the_title() . '</a></h4>';
								$product = new WC_Product( get_the_ID() );
								
							echo '</div>';
								echo '</div>';
								echo '</div>';

									endwhile;

									wp_reset_postdata();
								echo '</div>';

							echo '</div>';

					endif;

				endif;

				echo '</div>';

				echo '</section>';

	endif;
}

if ( ! function_exists( 'tradiestandard_search_products_no_results_wrapper' ) ) {
	/**
	 * No results on search wrapper start.
	 */
	function tradiestandard_search_products_no_results_wrapper() {

		$tradiestandard_body_classes = get_body_class();

		if ( is_search() && in_array( 'woocommerce',$tradiestandard_body_classes ) && in_array( 'search-no-results',$tradiestandard_body_classes ) ) {
			echo '<section class="module-small module-small-shop">';
				echo '<div class="container">';
		}
	}
}

if ( ! function_exists( 'tradiestandard_search_products_no_results_wrapper_end' ) ) {
	/**
	 * No results on search wrapper end.
	 */
	function tradiestandard_search_products_no_results_wrapper_end() {

		$tradiestandard_body_classes = get_body_class();

		if ( is_search() && in_array( 'woocommerce',$tradiestandard_body_classes ) && in_array( 'search-no-results',$tradiestandard_body_classes ) ) {
				echo '</div><!-- .container -->';
			echo '</section><!-- .module-small -->';
		}
	}
}

if ( ! function_exists( 'tradiestandard_loop_product_thumbnail' ) ) {
	/**
	 * Get the product thumbnail, or the placeholder if not set.
	 */
	function tradiestandard_loop_product_thumbnail() {
		global $product;
		if( is_product() ) {
			$image_size = 'full';
		} else {
			$image_size = 'shop_catalog';
		}

		if ( function_exists( 'method_exists' ) && method_exists( $product, 'get_gallery_image_ids' ) ) {
			$tradiestandard_gallery_attachment_ids = $product->get_gallery_image_ids();
		} elseif ( function_exists( 'method_exists' ) && method_exists( $product, 'get_gallery_attachment_ids' ) ) {
			$tradiestandard_gallery_attachment_ids = $product->get_gallery_attachment_ids();
		}

		if ( has_post_thumbnail() ) {
			if ( function_exists( 'wc_get_product_attachment_props' ) ) {

				$props = wc_get_product_attachment_props( get_post_thumbnail_id(), $product );
				$product_id = get_the_ID();
				echo get_the_post_thumbnail( $product_id, $image_size, array(
					'title'	 => $props['title'],
					'alt'    => $props['alt'],
				));
			}

			// if ( ! empty( $tradiestandard_gallery_attachment_ids[0] ) ) :
			// 	echo wp_get_attachment_image( $tradiestandard_gallery_attachment_ids[0], $image_size );
			// endif;

		} elseif ( ! empty( $tradiestandard_gallery_attachment_ids ) ) {

			if ( ! empty( $tradiestandard_gallery_attachment_ids[0] ) ) :
				echo wp_get_attachment_image( $tradiestandard_gallery_attachment_ids[0], $image_size );
			endif;

			if ( ! empty( $tradiestandard_gallery_attachment_ids[1] ) ) :
				echo wp_get_attachment_image( $tradiestandard_gallery_attachment_ids[1], $image_size );
			endif;

		} elseif ( function_exists( 'wc_placeholder_img_src' ) ) {

			$tradiestandard_placeholder_img = wc_placeholder_img( $image_size );

			if ( ! empty( $tradiestandard_placeholder_img ) ) {
				echo $tradiestandard_placeholder_img;
			}
		}
	}
}// End if().


if ( ! function_exists( 'tradiestandard_woocommerce_taxonomy_archive_description' ) ) {
	/**
	 * Display WooCommerce product category description on all category archive pages
	 */
	function tradiestandard_woocommerce_taxonomy_archive_description() {
		if ( is_tax( array( 'product_cat', 'product_tag' ) ) && get_query_var( 'paged' ) != 0 ) {
			$description = wc_format_content( term_description() );
			if ( $description ) {
				echo '<div class="term-description">' . $description . '</div>';
			}
		}
	}
}
add_action( 'woocommerce_archive_description', 'tradiestandard_woocommerce_taxonomy_archive_description' );


if ( ! function_exists( 'tradiestandard_woocommerce_product_archive_description' ) ) {
	/**
	 * Display WooCommerce shop content on all shop pages
	 */
	function tradiestandard_woocommerce_product_archive_description() {
		if ( is_post_type_archive( 'product' ) ) {
			$shop_page   = get_post( wc_get_page_id( 'shop' ) );
			if ( $shop_page ) {
				$description = wc_format_content( $shop_page->post_content );
				if ( $description ) {
					echo '<div class="page-description">' . $description . '</div>';
				}
			}
		}
	}
}

/**
 * Fix for Shop page set up as static frontpage problem with the sidebar
 */
function tradiestandard_woocommerce_breadcrumb() {
	if ( function_exists( 'woocommerce_breadcrumb' ) ) {
		if ( is_front_page() ) {
			echo '<nav class="woocommerce-breadcrumb"><a href="' . esc_url( home_url( '/' ) ) . '">Home</a></nav>';
		} else {
			woocommerce_breadcrumb();
		}
	}
}

/**
 * Function to add product banner
 * added via metabox
 */
if ( ! function_exists( 'tradiestandard_loop_product_banner' ) ) {
	function tradiestandard_loop_product_banner() {
		$top_banner = get_post_meta(get_the_ID(), 'rask_ps_top_banner_id', 1);
		$terms = wp_get_post_terms( get_the_ID(), 'product_cat', array('orderby'=>'menu_order', 'order'=>'DESC') );
		if($top_banner) {
			$product_banner = wp_get_attachment_image( $top_banner, 'full' );
			echo '<div class="col-sm-12 col-md-12 product-banner">' . $product_banner . '</div>';
		} elseif( $terms && ! is_wp_error($terms) ) {
				foreach ($terms as $key => $category) {
					$banner_id = get_term_meta( $category->term_id, 'rask_term_banner_id', 1 );
					if($banner_id) {
						$product_banner = wp_get_attachment_image( $banner_id, 'full' );
						echo '<div class="col-sm-12 col-md-12 product-banner">' . $product_banner . '</div>';
						break;
					}
				}
		} else {
			$product_banner = ' ';
		}
	}
}

/**
 * Function to add description in woocommerce_single_product_summary action
 * 
 */

function tradiestandard_product_description() {
 wc_get_template( 'single-product/description.php' );
}

/**
 * Remove default tab
 * Add Custom tab, product page
 * custom repeatable field from product page
 */
add_filter( 'woocommerce_product_tabs', 'tradiestandard_product_tab' );
function tradiestandard_product_tab( $tabs ) {

	unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab
	
	// Adds the new tab
	
    $entries = get_post_meta( get_the_ID(), 'repeatable_tab_sections', true );
    $counter = 1;
	foreach ( (array) $entries as $key => $entry ) {
	$counter++;
		$tab_content = $tab_title = '';

		if ( isset( $entry['tab_title'] ) ) {
			$title = esc_html( $entry['tab_title'] );
		}
		if ( isset( $entry['tab_content'] ) ) {
			$desc = wpautop( $entry['tab_content'] );
		}
		if($title) {
			$tabs['tradiestandard_product_tab_'.$counter] = array(
				'title' 	=> __( $title, 'tradiestandard' ),
				'priority' 	=> 50+$counter,
				'tabContent' => $desc,
				'callback' 	=> 'tradiestandard_product_tab_content_'
			);
		}

	}
	return $tabs;

}

function tradiestandard_product_tab_content_( $tab_key, $tab_info ) {
	echo apply_filters( 'tab_content', $tab_info['tabContent'] );
=======
<?php
/**
 * General functions used to integrate this theme with WooCommerce.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */


if ( ! function_exists( 'tradiestandard_before_content' ) ) {
	/**
	 * Before Content
	 * Wraps all WooCommerce content in wrappers which match the theme markup
	 *
	 * @since   1.0.0
	 * @return  void
	 */
	function tradiestandard_before_content() {
		?>
		<div class="main">
			<?php
	}
}

if ( ! function_exists( 'tradiestandard_after_content' ) ) {
	/**
	 * After Content
	 * Closes the wrapping divs
	 *
	 * @since   1.0.0
	 * @return  void
	 */
	function tradiestandard_after_content() {
		?>
		</div><!-- .main -->

		<?php
	}
}

if ( ! function_exists( 'tradiestandard_shop_page_wrapper' ) ) {

	/**
	 * Before Shop loop
	 *
	 * @since   1.0.0
	 * @return  void
	 */
	function tradiestandard_shop_page_wrapper() {
		?>
		<section class="module-small module-small-shop">
				<div class="container">

				<?php if ( is_shop() || is_product_tag() || is_product_category() ) :

						do_action( 'tradiestandard_before_shop' );

					if ( is_active_sidebar( 'tradiestandard-sidebar-shop-archive' ) ) : ?>

							<div class="col-sm-9 shop-with-sidebar" id="tradiestandard-blog-container">

						<?php endif; ?>

				<?php endif; ?>

		<?php
	}
}

/**
 * Before Product content
 *
 * @since   1.0.0
 * @return  void
 */
function tradiestandard_product_page_wrapper() {
	echo '<section class="module module-super-small">
			<div class="container product-main-content">';
}

if ( ! function_exists( 'tradiestandard_product_page_wrapper_end' ) ) {
	/**
	 * After Product content
	 * Closes the wrapping div and section
	 *
	 * @since   1.0.0
	 * @return  void
	 */
	function tradiestandard_product_page_wrapper_end() {
		?>
			</div><!-- .container -->
		</section><!-- .module-small -->
			<?php
	}
}

if ( ! function_exists( 'tradiestandard_shop_page_wrapper_end' ) ) {
	/**
	 * After Shop loop
	 * Closes the wrapping div and section
	 *
	 * @since   1.0.0
	 * @return  void
	 */
	function tradiestandard_shop_page_wrapper_end() {
		?>

			<?php if ( (is_shop() || is_product_category() || is_product_tag() ) && is_active_sidebar( 'tradiestandard-sidebar-shop-archive' ) ) :  ?>

				</div>

				<!-- Sidebar column start -->
				<div class="col-sm-3 col-md-3 sidebar sidebar-shop">
					<?php do_action( 'tradiestandard_sidebar_shop_archive' ); ?>
				</div>
				<!-- Sidebar column end -->

			<?php endif; ?>

			</div><!-- .container -->
		</section><!-- .module-small -->
		<?php
	}
}

/**
 * Default loop columns on product archives
 *
 * @return integer products per row
 * @since  1.0.0
 */
function tradiestandard_loop_columns() {
	if ( is_active_sidebar( 'tradiestandard-sidebar-shop-archive' ) ) {
		return apply_filters( 'tradiestandard_loop_columns', 3 ); // 3 products per row
	} else {
		return apply_filters( 'tradiestandard_loop_columns', 4 ); // 4 products per row
	}
}

/**
 * Add 'woocommerce-active' class to the body tag
 *
 * @param  array $classes body classes.
 *
 * @return array $classes modified to include 'woocommerce-active' class.
 */
function tradiestandard_woocommerce_body_class( $classes ) {
	if ( is_woocommerce_activated() ) {
		$classes[] = 'woocommerce-active';
	}

	return $classes;
}

/**
 * WooCommerce specific scripts & stylesheets
 *
 * @since 1.0.0
 */
function tradiestandard_woocommerce_scripts() {

	wp_enqueue_style( 'tradiestandard-woocommerce-style1', get_template_directory_uri() . '/inc/woocommerce/css/woocommerce.css', array(), 'v3' );
}

/**
 * Related Products Args
 *
 * @param  array $args related products args.
 *
 * @since 1.0.0
 * @return  array $args related products args.
 */
function tradiestandard_related_products_args( $args ) {
	$args = apply_filters( 'tradiestandard_related_products_args', array(
		'posts_per_page' => 4,
		'columns'        => 4,
	) );

	return $args;
}

/**
 * Product gallery thumnail columns
 *
 * @return integer number of columns
 * @since  1.0.0
 */
function tradiestandard_thumbnail_columns() {
	return intval( apply_filters( 'tradiestandard_product_thumbnail_columns', 4 ) );
}

/**
 * Products per page
 *
 * @return integer number of products
 * @since  1.0.0
 */
function tradiestandard_products_per_page() {
	return intval( apply_filters( 'tradiestandard_products_per_page', 12 ) );
}

/**
 * Header for shop page
 *
 * @since  1.0.0
 */
function tradiestandard_header_shop_page( $page_title ) {

	$tradiestandard_title = '';

	$tradiestandard_header_image = get_header_image();
	if ( ! empty( $tradiestandard_header_image ) ) :
		$tradiestandard_title = '<section class="' . ( is_woocommerce() ? 'woocommerce-page-title ' : '' ) . 'page-header-module module bg-dark" data-background="' . $tradiestandard_header_image . '">';
	else :
		$tradiestandard_title = '<section class="page-header-module module bg-dark">';
	endif;

		$tradiestandard_title .= '<div class="container">';

			$tradiestandard_title .= '<div class="row">';

				$tradiestandard_title .= '<div class="col-sm-6 col-sm-offset-3">';

	if ( ! empty( $page_title ) ) :

		$tradiestandard_title .= '<h1 class="module-title font-alt">' . $page_title . '</h1>';

					endif;

				$tradiestandard_title .= '</div>';

			$tradiestandard_title .= '</div><!-- .row -->';

		$tradiestandard_title .= '</div>';
	$tradiestandard_title .= '</section>';

	return $tradiestandard_title;
}

/**
 * Products slider on single page product
 *
 * @since  1.0.0
 */
function tradiestandard_products_slider_on_single_page() {

	$tradiestandard_products_slider_single_hide = get_theme_mod( 'tradiestandard_products_slider_single_hide' );

	if ( isset( $tradiestandard_products_slider_single_hide ) && $tradiestandard_products_slider_single_hide != 1 ) :
		echo '<hr class="divider-w">';
		echo '<section class="module module-small-bottom aya">';
	elseif ( is_customize_preview() ) :
		echo '<hr class="divider-w">';
		echo '<section class="module module-small-bottom tradiestandard_hidden_if_not_customizer">';
	endif;

	if ( ( isset( $tradiestandard_products_slider_single_hide ) && $tradiestandard_products_slider_single_hide != 1 ) || is_customize_preview() ) :

			echo '<div class="container">';

				$tradiestandard_products_slider_title = get_theme_mod( 'tradiestandard_products_slider_title',__( 'Exclusive products', 'tradiestandard' ) );
				$tradiestandard_products_slider_subtitle = get_theme_mod( 'tradiestandard_products_slider_subtitle',__( 'Special category of products', 'tradiestandard' ) );

		if ( ! empty( $tradiestandard_products_slider_title ) || ! empty( $tradiestandard_products_slider_subtitle ) ) :
			echo '<div class="row">';
			echo '<div class="col-sm-6 col-sm-offset-3">';
			if ( ! empty( $tradiestandard_products_slider_title ) ) :
				echo '<h2 class="module-title font-alt">' . $tradiestandard_products_slider_title . '</h2>';
			endif;
			if ( ! empty( $tradiestandard_products_slider_subtitle ) ) :
				echo '<div class="module-subtitle font-serif">' . $tradiestandard_products_slider_subtitle . '</div>';
			endif;
			echo '</div>';
			echo '</div><!-- .row -->';
				endif;

				$tradiestandard_products_slider_category = get_theme_mod( 'tradiestandard_products_slider_category' );

		$tax_query_item = array();
		$meta_query_item = array();
		if ( taxonomy_exists( 'product_visibility' ) ) {
			$tax_query_item = array(
				array(
					'taxonomy' => 'product_visibility',
					'field'    => 'term_id',
					'terms'    => 'exclude-from-catalog',
					'operator' => 'NOT IN',
				),
			);
		} else {
			$meta_query_item = array(
				'key'     => '_visibility',
				'value'   => 'hidden',
				'compare' => '!=',
			);
		}

		$tradiestandard_products_slider_args = array(
				'post_type' => 'product',
				'posts_per_page' => 10,
		);

		if ( ! empty( $tradiestandard_products_slider_category ) && ($tradiestandard_products_slider_category != '-') ) {
			$tradiestandard_products_slider_args['tax_query'] = array(
				array(
					'taxonomy'   => 'product_cat',
					'field'      => 'term_id',
					'terms'      => $tradiestandard_products_slider_category,
				),
			);
		}

		if ( ! empty( $tax_query_item ) ) {
			$tradiestandard_products_slider_args['tax_query']['relation'] = 'AND';
			$tradiestandard_products_slider_args['tax_query'] = array_merge( $tradiestandard_products_slider_args['tax_query'],$tax_query_item );
		}

		if ( ! empty( $meta_query_item ) ) {
			$tradiestandard_products_slider_args['meta_query'] = $meta_query_item;
		}

		if ( ! empty( $tradiestandard_products_slider_category ) && ($tradiestandard_products_slider_category != '-') ) :

			$tradiestandard_products_slider_loop = new WP_Query( $tradiestandard_products_slider_args );

			if ( $tradiestandard_products_slider_loop->have_posts() ) :

				$rtl_slider = apply_filters( 'tradiestandard_products_slider_single_rtl', 'false' );
				$number_of_items = apply_filters( 'tradiestandard_products_slider_single_items', 5 );
				$pagination = apply_filters( 'tradiestandard_products_slider_single_pagination', 'false' );
				$navigation = apply_filters( 'tradiestandard_products_slider_single_navigation', 'false' );

				echo '<div class="row">';

				echo '<div id="prod_slider" class="owl-carousel text-center" data-items="' . esc_attr( $number_of_items ) . '" data-pagination="' . esc_attr( $pagination ) . '" data-navigation="' . esc_attr( $navigation ) . '" data-rtl="' . esc_attr( $rtl_slider ) . '" >';

				while ( $tradiestandard_products_slider_loop->have_posts() ) :

					$tradiestandard_products_slider_loop->the_post();

					echo '<div class="owl-item">';
					echo '<div class="col-sm-12">';
					echo '<div class="ex-product">';
					echo '<a href="' . esc_url( get_permalink() ) . '">' . woocommerce_get_product_thumbnail() . '</a>';
					echo '<h4 class="shop-item-title font-alt"><a href="' . esc_url( get_permalink() ) . '">' . get_the_title() . '</a></h4>';
					$product = new WC_Product( get_the_ID() );

					echo '</div>';
					echo '</div>';
					echo '</div>';

					endwhile;

					wp_reset_postdata();
				echo '</div>';

					echo '</div>';

			endif;

				else :

					$tradiestandard_products_slider_loop = new WP_Query( $tradiestandard_products_slider_args );

					if ( $tradiestandard_products_slider_loop->have_posts() ) :

						$rtl_slider = apply_filters( 'tradiestandard_products_slider_single_rtl', 'false' );
						$number_of_items = apply_filters( 'tradiestandard_products_slider_single_items', 5 );
						$pagination = apply_filters( 'tradiestandard_products_slider_single_pagination', 'false' );
						$navigation = apply_filters( 'tradiestandard_products_slider_single_navigation', 'false' );

						echo '<div class="row">';

						echo '<div class="owl-carousel text-center" data-items="' . esc_attr( $number_of_items ) . '" data-pagination="' . esc_attr( $pagination ) . '" data-navigation="' . esc_attr( $navigation ) . '" data-rtl="' . esc_attr( $rtl_slider ) . '" >';

						while ( $tradiestandard_products_slider_loop->have_posts() ) :

							$tradiestandard_products_slider_loop->the_post();

							echo '<div class="owl-item">';
							echo '<div class="col-sm-12">';
							echo '<div class="ex-product">';
							echo '<a href="' . esc_url( get_permalink() ) . '">' . woocommerce_get_product_thumbnail() . '</a>';
							echo '<h4 class="shop-item-title font-alt"><a href="' . esc_url( get_permalink() ) . '">' . get_the_title() . '</a></h4>';
								$product = new WC_Product( get_the_ID() );
								
							echo '</div>';
								echo '</div>';
								echo '</div>';

									endwhile;

									wp_reset_postdata();
								echo '</div>';

							echo '</div>';

					endif;

				endif;

				echo '</div>';

				echo '</section>';

	endif;
}

if ( ! function_exists( 'tradiestandard_search_products_no_results_wrapper' ) ) {
	/**
	 * No results on search wrapper start.
	 */
	function tradiestandard_search_products_no_results_wrapper() {

		$tradiestandard_body_classes = get_body_class();

		if ( is_search() && in_array( 'woocommerce',$tradiestandard_body_classes ) && in_array( 'search-no-results',$tradiestandard_body_classes ) ) {
			echo '<section class="module-small module-small-shop">';
				echo '<div class="container">';
		}
	}
}

if ( ! function_exists( 'tradiestandard_search_products_no_results_wrapper_end' ) ) {
	/**
	 * No results on search wrapper end.
	 */
	function tradiestandard_search_products_no_results_wrapper_end() {

		$tradiestandard_body_classes = get_body_class();

		if ( is_search() && in_array( 'woocommerce',$tradiestandard_body_classes ) && in_array( 'search-no-results',$tradiestandard_body_classes ) ) {
				echo '</div><!-- .container -->';
			echo '</section><!-- .module-small -->';
		}
	}
}

if ( ! function_exists( 'tradiestandard_loop_product_thumbnail' ) ) {
	/**
	 * Get the product thumbnail, or the placeholder if not set.
	 */
	function tradiestandard_loop_product_thumbnail() {
		global $product;
		if( is_product() ) {
			$image_size = 'full';
		} else {
			$image_size = 'shop_catalog';
		}

		if ( function_exists( 'method_exists' ) && method_exists( $product, 'get_gallery_image_ids' ) ) {
			$tradiestandard_gallery_attachment_ids = $product->get_gallery_image_ids();
		} elseif ( function_exists( 'method_exists' ) && method_exists( $product, 'get_gallery_attachment_ids' ) ) {
			$tradiestandard_gallery_attachment_ids = $product->get_gallery_attachment_ids();
		}

		if ( has_post_thumbnail() ) {
			if ( function_exists( 'wc_get_product_attachment_props' ) ) {

				$props = wc_get_product_attachment_props( get_post_thumbnail_id(), $product );
				$product_id = get_the_ID();
				echo get_the_post_thumbnail( $product_id, $image_size, array(
					'title'	 => $props['title'],
					'alt'    => $props['alt'],
				));
			}

			// if ( ! empty( $tradiestandard_gallery_attachment_ids[0] ) ) :
			// 	echo wp_get_attachment_image( $tradiestandard_gallery_attachment_ids[0], $image_size );
			// endif;

		} elseif ( ! empty( $tradiestandard_gallery_attachment_ids ) ) {

			if ( ! empty( $tradiestandard_gallery_attachment_ids[0] ) ) :
				echo wp_get_attachment_image( $tradiestandard_gallery_attachment_ids[0], $image_size );
			endif;

			if ( ! empty( $tradiestandard_gallery_attachment_ids[1] ) ) :
				echo wp_get_attachment_image( $tradiestandard_gallery_attachment_ids[1], $image_size );
			endif;

		} elseif ( function_exists( 'wc_placeholder_img_src' ) ) {

			$tradiestandard_placeholder_img = wc_placeholder_img( $image_size );

			if ( ! empty( $tradiestandard_placeholder_img ) ) {
				echo $tradiestandard_placeholder_img;
			}
		}
	}
}// End if().


if ( ! function_exists( 'tradiestandard_woocommerce_taxonomy_archive_description' ) ) {
	/**
	 * Display WooCommerce product category description on all category archive pages
	 */
	function tradiestandard_woocommerce_taxonomy_archive_description() {
		if ( is_tax( array( 'product_cat', 'product_tag' ) ) && get_query_var( 'paged' ) != 0 ) {
			$description = wc_format_content( term_description() );
			if ( $description ) {
				echo '<div class="term-description">' . $description . '</div>';
			}
		}
	}
}
add_action( 'woocommerce_archive_description', 'tradiestandard_woocommerce_taxonomy_archive_description' );


if ( ! function_exists( 'tradiestandard_woocommerce_product_archive_description' ) ) {
	/**
	 * Display WooCommerce shop content on all shop pages
	 */
	function tradiestandard_woocommerce_product_archive_description() {
		if ( is_post_type_archive( 'product' ) ) {
			$shop_page   = get_post( wc_get_page_id( 'shop' ) );
			if ( $shop_page ) {
				$description = wc_format_content( $shop_page->post_content );
				if ( $description ) {
					echo '<div class="page-description">' . $description . '</div>';
				}
			}
		}
	}
}

/**
 * Fix for Shop page set up as static frontpage problem with the sidebar
 */
function tradiestandard_woocommerce_breadcrumb() {
	if ( function_exists( 'woocommerce_breadcrumb' ) ) {
		if ( is_front_page() ) {
			echo '<nav class="woocommerce-breadcrumb"><a href="' . esc_url( home_url( '/' ) ) . '">Home</a></nav>';
		} else {
			woocommerce_breadcrumb();
		}
	}
}

/**
 * Function to add product banner
 * added via metabox
 */
if ( ! function_exists( 'tradiestandard_loop_product_banner' ) ) {
	function tradiestandard_loop_product_banner() {
		$top_banner = get_post_meta(get_the_ID(), 'rask_ps_top_banner_id', 1);
		$terms = wp_get_post_terms( get_the_ID(), 'product_cat', array('orderby'=>'menu_order', 'order'=>'DESC') );
		if($top_banner) {
			$product_banner = wp_get_attachment_image( $top_banner, 'full' );
			echo '<div class="col-sm-12 col-md-12 product-banner">' . $product_banner . '</div>';
		} elseif( $terms && ! is_wp_error($terms) ) {
				foreach ($terms as $key => $category) {
					$banner_id = get_term_meta( $category->term_id, 'rask_term_banner_id', 1 );
					if($banner_id) {
						$product_banner = wp_get_attachment_image( $banner_id, 'full' );
						echo '<div class="col-sm-12 col-md-12 product-banner">' . $product_banner . '</div>';
						break;
					}
				}
		} else {
			$product_banner = ' ';
		}
	}
}

/**
 * Function to add description in woocommerce_single_product_summary action
 * 
 */

function tradiestandard_product_description() {
 wc_get_template( 'single-product/description.php' );
}

/**
 * Remove default tab
 * Add Custom tab, product page
 * custom repeatable field from product page
 */
add_filter( 'woocommerce_product_tabs', 'tradiestandard_product_tab' );
function tradiestandard_product_tab( $tabs ) {

	unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab
	
	// Adds the new tab
	
    $entries = get_post_meta( get_the_ID(), 'repeatable_tab_sections', true );
    $counter = 1;
	foreach ( (array) $entries as $key => $entry ) {
	$counter++;
		$tab_content = $tab_title = '';

		if ( isset( $entry['tab_title'] ) ) {
			$title = esc_html( $entry['tab_title'] );
		}
		if ( isset( $entry['tab_content'] ) ) {
			$desc = wpautop( $entry['tab_content'] );
		}
		if($title) {
			$tabs['tradiestandard_product_tab_'.$counter] = array(
				'title' 	=> __( $title, 'tradiestandard' ),
				'priority' 	=> 50+$counter,
				'tabContent' => $desc,
				'callback' 	=> 'tradiestandard_product_tab_content_'
			);
		}

	}
	return $tabs;

}

function tradiestandard_product_tab_content_( $tab_key, $tab_info ) {
	echo apply_filters( 'tab_content', $tab_info['tabContent'] );
>>>>>>> 2f60927d06dd186fefafb15442a32abd421ec7aa
}