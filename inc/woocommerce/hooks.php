<?php
/**
 * tradiestandard WooCommerce Hooks
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

/**
 * Styles
 *
 * @see  tradiestandard_woocommerce_scripts()
 */
add_action( 'wp_enqueue_scripts', 			'tradiestandard_woocommerce_scripts',		20 );
add_filter( 'woocommerce_enqueue_styles', 	'__return_empty_array' );

/**
 * Layout
 *
 * @see  tradiestandard_before_content()
 * @see  tradiestandard_after_content()
 * @see  woocommerce_breadcrumb()
 * @see  tradiestandard_shop_messages()
 */

remove_action( 'woocommerce_before_main_content', 	'woocommerce_breadcrumb', 					20, 0 );
remove_action( 'woocommerce_before_main_content', 	'woocommerce_output_content_wrapper', 		10 );
remove_action( 'woocommerce_after_main_content', 	'woocommerce_output_content_wrapper_end', 	10 );
remove_action( 'woocommerce_sidebar', 				'woocommerce_get_sidebar', 					10 );
remove_action( 'woocommerce_before_shop_loop', 		'woocommerce_result_count', 				20 );
remove_action( 'woocommerce_before_shop_loop', 		'woocommerce_catalog_ordering', 			30 );
remove_action( 'woocommerce_after_shop_loop', 		'woocommerce_pagination', 					10 );
remove_action( 'woocommerce_archive_description', 	'woocommerce_product_archive_description',  10 );



remove_action( 'woocommerce_before_shop_loop_item_title',   'woocommerce_template_loop_product_thumbnail', 10 );
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);

add_action( 'woocommerce_before_shop_loop_item_title',   'tradiestandard_loop_product_thumbnail', 20 );

/**
	remove add to cart button
*/
	remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');



add_action( 'tradiestandard_before_shop', 				'tradiestandard_woocommerce_product_archive_description', 	5 );

add_action( 'woocommerce_before_main_content', 		'tradiestandard_before_content', 				10 );

add_action( 'woocommerce_before_shop_loop', 		'tradiestandard_shop_page_wrapper', 				20 );

add_action( 'tradiestandard_content_top', 				'tradiestandard_shop_messages', 				    21 );

add_action( 'woocommerce_after_shop_loop',			'tradiestandard_sorting_wrapper',				23 );
add_action( 'woocommerce_after_shop_loop', 			'tradiestandard_woocommerce_pagination', 		24 );
add_action( 'woocommerce_after_shop_loop',			'tradiestandard_sorting_wrapper_close',			25 );
add_action( 'woocommerce_after_shop_loop', 			'tradiestandard_shop_page_wrapper_end', 			40 );

add_action( 'woocommerce_after_main_content', 		'tradiestandard_after_content', 				    50 );

add_filter( 'woocommerce_page_title', 'tradiestandard_header_shop_page' );







/* WooCommerce Search Products Page - No results */
add_action( 'woocommerce_archive_description',              'tradiestandard_search_products_no_results_wrapper',      10 );
add_action( 'woocommerce_after_main_content',               'tradiestandard_search_products_no_results_wrapper_end',  10 );

/**
 * Products
 *
 */
remove_action( 'woocommerce_before_single_product',         'action_woocommerce_before_single_product', 10, 1 );
remove_action( 'woocommerce_after_single_product',          'action_woocommerce_after_single_product', 10, 1 );

add_action( 'woocommerce_before_single_product',            'tradiestandard_product_page_wrapper', 10, 1 );
add_action( 'woocommerce_before_single_product',            'woocommerce_breadcrumb', 11 );
add_action( 'woocommerce_after_single_product',             'tradiestandard_product_page_wrapper_end', 10, 1 );

remove_action( 'woocommerce_before_shop_loop_item_title', 	'woocommerce_show_product_loop_sale_flash', 10 );

/* add products slider */
add_action( 'woocommerce_after_single_product',             'tradiestandard_products_slider_on_single_page', 10, 0 );

/* notices */
remove_action( 'woocommerce_before_single_product',         'wc_print_notices', 10 );
add_action( 'woocommerce_before_single_product',            'wc_print_notices', 60 );

/**
 * Filters
 *
 * @see  tradiestandard_woocommerce_body_class()
 * @see  tradiestandard_thumbnail_columns()
 * @see  tradiestandard_related_products_args()
 * @see  tradiestandard_products_per_page()
 * @see  tradiestandard_loop_columns()
 */
add_filter( 'body_class', 								'tradiestandard_woocommerce_body_class' );
add_filter( 'woocommerce_product_thumbnails_columns', 	'tradiestandard_thumbnail_columns' );
add_filter( 'woocommerce_output_related_products_args', 'tradiestandard_related_products_args' );
add_filter( 'loop_shop_per_page', 						'tradiestandard_products_per_page' );
add_filter( 'loop_shop_columns', 						'tradiestandard_loop_columns' );


add_action( 'woocommerce_before_single_product_summary',   'tradiestandard_loop_product_banner', 10 );
remove_action( 'woocommerce_before_single_product_summary',   'woocommerce_show_product_images', 20 );
add_action('woocommerce_new_action_hook_product_slider', 'woocommerce_show_product_images', 10);



/**
 * single product page
 * remove rating
 * remove short description
 * remove price
 * remove cart
 * remove meta
 * remove sharing
*/
remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
add_action('woocommerce_single_product_summary', 'tradiestandard_product_description', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

//add product excerpt below slider/thumb
add_action('woocommerce_new_action_hook_product_slider', 'woocommerce_template_single_excerpt', 15);
