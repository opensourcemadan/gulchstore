<?php
/**
 * Custom template tags used to integrate this theme with WooCommerce.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

/**
 * Sorting wrapper
 *
 * @since   1.4.3
 * @return  void
 */
function tradiestandard_sorting_wrapper() {
	echo '<div class="row">';
	echo '<div class="col-sm-12">';
}

/**
 * Sorting wrapper close
 *
 * @since   1.4.3
 * @return  void
 */
function tradiestandard_sorting_wrapper_close() {
	echo '</div>';
	echo '</div>';
}

/**
 * tradiestandard shop messages
 *
 * @since   1.4.4
 * @uses    do_shortcode
 */
function tradiestandard_shop_messages() {
	if ( ! is_checkout() ) {
		echo wp_kses_post( do_shortcode( '[woocommerce_messages]' ) );
	}
}

if ( ! function_exists( 'tradiestandard_woocommerce_pagination' ) ) {

	/**
	 * Pagination on shop page
	 *
	 * @since  1.0.0
	 */
	function tradiestandard_woocommerce_pagination() {
		if ( woocommerce_products_will_display() ) {
			woocommerce_pagination();
		}
	}
}
