<<<<<<< HEAD
/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {

	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).text( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description a' ).text( to );
		} );
	} );

	/****************************************/
	/********** Big title section ***********/
	/****************************************/
	wp.customize( 'tradiestandard_big_title_hide', function( value ) {
		value.bind( function( to ) {
			if( to !== true ) {
				$( '.home-section' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			} else {
				$( '.home-section' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}

		} );
	} );

	/******************************/
	/**********  Colors ***********/
	/******************************/

	wp.customize( 'header_textcolor', function( value ) {
		value.bind( function( to ) {
			$( '.tradiestandard_header_title h1 a' ).css( {
				'color': to
			} );
			$( '.tradiestandard_header_title h2 a' ).css( {
				'color': to
			} );
		} );
	} );
	wp.customize( 'background_color', function( value ) {
		value.bind( function( to ) {
			$( 'body' ).css( {
				'background': to
			} );
			if( $( '.front-page-main' ).length > 0 ) { 
				$( '.front-page-main' ).css( {
					'background': to
				} );
			}
		} );
	} );

    /******************************/
    /**********  Header ***********/
    /******************************/
    wp.customize( 'tradiestandard_logo', function( value ) {
        value.bind( function( to ) {

            if( to !== '' ) {
                $( '.tradiestandard_header_title .logo-image' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
                $( '.tradiestandard_header_title h1' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
                $( '.tradiestandard_header_title h2' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
            }
            else {
                $( '.tradiestandard_header_title .logo-image' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
                $( '.tradiestandard_header_title h1' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
                $( '.tradiestandard_header_title h2' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
            }
            $( '.tradiestandard_header_title img' ).attr( 'src', to );

        } );
    } );

    wp.customize( 'tradiestandard_blog_header_title', function( value ) {
        value.bind( function( to ) {
            if( to !== '' ) {
                $( '.tradiestandard-blog-header-title' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
            }
            else {
                $( '.tradiestandard-blog-header-title' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
            }
            $( '.tradiestandard-blog-header-title' ).html( to );
        } );
    } );

    wp.customize( 'tradiestandard_blog_header_subtitle', function( value ) {
        value.bind( function( to ) {
            if( to !== '' ) {
                $( '.tradiestandard-blog-header-subtitle' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
            }
            else {
                $( '.tradiestandard-blog-header-subtitle' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
            }
            $( '.tradiestandard-blog-header-subtitle' ).html( to );
        } );
    } );
	
	/*******************************/
	/******    Slider section ******/
	/*******************************/
	wp.customize( 'tradiestandard_slider_hide', function( value ) {
		value.bind( function( to ) {
			if( to !== '1' ) {
				$( 'section.home-section' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
				$( '.navbar-custom' ).removeClass( 'navbar-color-customizer' );
				$('.main').css('margin-top', 0 );
			}
			else {
				$( 'section.home-section' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
				$( '.navbar-custom' ).addClass( 'navbar-color-customizer' );
				$('.main').css('margin-top', $('.navbar-custom').outerHeight() );
			}
		} );
	} );

	/********************************/
    /*********	Banners section *****/
	/********************************/
	wp.customize( 'tradiestandard_banners_hide', function( value ) {
		value.bind( function( to ) {
			if( to !== true ) {
				$( '.home-banners' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '.home-banners' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}
		} );
	} );

	wp.customize( 'tradiestandard_banners_title', function( value ) {

		value.bind( function( to ) {
			if( to !== '' ) {
				$( '.product-banners-title' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '.product-banners-title' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			$( '.product-banners-title' ).text( to );
		} );

	} );

	// Add new banner (Repeater)
	wp.customize( 'tradiestandard_banners', function( value ) {
		value.bind( function( to ) {
			var obj = JSON.parse( to );
			var result ='';
			obj.forEach(function(item) {
				result += '<div class="col-sm-4"><div class="content-box mt-0 mb-0"><div class="content-box-image"><a href="' + item.link + '"><img src="' + item.image_url + '"></a></div></div></div>';
			});
			$( '.tradiestandard_bannerss_section' ).html( result );
		} );
	} );



	/*********************************/
    /*******  Products section *******/
	/********************************/
	wp.customize( 'tradiestandard_products_hide', function( value ) {
		value.bind( function( to ) {
			if( to !== true ) {
				$( '#latest' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '#latest' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}
		} );
	} );
	wp.customize( 'tradiestandard_products_title', function( value ) {
		value.bind( function( to ) {
			$( '.product-hide-title' ).html( to );
		} );
	} );

	/****************************************/
	/*********** Video section **************/
	/****************************************/
	wp.customize( 'tradiestandard_video_hide', function( value ) {
		value.bind( function( to ) {
			if( to !== true ) {
				$( '.module-video' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '.module-video' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}
		} );
	} );
	wp.customize( 'tradiestandard_video_title', function( value ) {
		value.bind( function( to ) {
			var $title = $( '.video-title' );
            if  ( to !== '' ) {
                $title.removeClass( 'tradiestandard_hidden_if_not_customizer' );
                $title.html( to );
            } else {
                $title.addClass('tradiestandard_hidden_if_not_customizer');
			}
		} );
	} );

    wp.customize('tradiestandard_video_section_padding', function (value) {
        var $selector = $('.module.module-video');
        value.bind(function (to) {
            $( window ).resize();
            $selector.css('padding', to+'px 0px');
        });
    });

	/****************************************/
    /*******  Products slider section *******/
	/****************************************/
	wp.customize( 'tradiestandard_products_slider_hide', function( value ) {
		value.bind( function( to ) {
			if( to !== true ) {
				$( '.home-product-slider' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '.home-product-slider' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}
		} );
	} );
	wp.customize( 'tradiestandard_products_slider_single_hide', function( value ) {
		value.bind( function( to ) {
			if( to !== true ) {
				$( '.module-small-bottom' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '.module-small-bottom' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}
		} );
	} );
	wp.customize( 'tradiestandard_products_slider_title', function( value ) {
		value.bind( function( to ) {
			$( '.home-prod-title' ).text( to );
		} );
	} );
	wp.customize( 'tradiestandard_products_slider_subtitle', function( value ) {
		value.bind( function( to ) {
			$( '.home-prod-subtitle' ).text( to );
		} );
	} );
	
	/****************************************/
	/*********  Services section ************/
	/****************************************/

	wp.customize( 'tradiestandard_services_title', function( value ) {
		value.bind( function( to ) {
			$( '.services h2' ).text( to );
		} );
	} );
	wp.customize( 'tradiestandard_services_subtitle', function( value ) {
		value.bind( function( to ) {
			$( '.services .module-subtitle' ).text( to );
		} );
	} );
	
	/* Our services (Repeater) */
	wp.customize( 'tradiestandard_service_box', function( value ) {
		value.bind( function( to ) {
			var obj = JSON.parse( to );
			var result ='';
			obj.forEach(function(item) {
				result += '<div class="col-xs-12 col-sm-4 sip-service-box"><div class="sip-single-service"><a href="'+item.link+'" class="sip-single-service-a"><span class="'+item.icon_value+' sip-single-icon"></span><div class="sip-single-service-text"><h3 class="">'+item.text+'</h3><p class="">'+item.subtext+'</p></div></a></div></div>';
			});
			$( '#our_services_wrap' ).html( result );
		} );
	} );

	/****************************************/
	/*********  Ribbon section **************/
	/****************************************/
	wp.customize( 'tradiestandard_ribbon_text', function( value ) {
		value.bind( function( to ) {
			if( to !== '1' ) {
				$( '.ribbon' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '.ribbon' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}
		} );
	} );
	wp.customize( 'tradiestandard_ribbon_text', function( value ) {
		value.bind( function( to ) {
			$( '.ribbon h2' ).text( to );
		} );
	} );

	wp.customize( 'tradiestandard_ribbon_button_text', function( value ) {
		value.bind( function( to ) {
			$( '.ribbon .btn-ribbon' ).text( to );
		} );
	} );

	wp.customize( 'tradiestandard_ribbon_background', function( value ) {
		value.bind( function( to ) {
			$( '#ribbon' ).css( 'background-image', 'url(' + to + ')' );
		} );
	} );

	/*****************************************/
	/*********  Categories section ***********/
	/*****************************************/

	wp.customize( 'tradiestandard_fp_categories_title', function( value ) {
		value.bind( function( to ) {
			$( '.categories#categories h2' ).text( to );
		} );
	} );
	wp.customize( 'tradiestandard_fp_categories_subtitle', function( value ) {
		value.bind( function( to ) {
			$( '.categories#categories .module-subtitle' ).text( to );
		} );
	} );

	/*******************************/
    /***********  Footer ***********/
	/*******************************/
	wp.customize( 'tradiestandard_copyright', function( value ) {
		value.bind( function( to ) {
			$( '.copyright' ).text( to );
		} );
	} );


	/*******************************************/
	/******    Hide site info from footer ******/
	/*******************************************/
	wp.customize( 'tradiestandard_site_info_hide', function( value ) {
		value.bind( function( to ) {
			if( to !== true ) {
				$( '.tradiestandard-poweredby-box' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '.tradiestandard-poweredby-box' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}
		} );
	} );

	// socials (Repeater)
	wp.customize( 'tradiestandard_socials', function( value ) {
		value.bind( function( to ) {
			var obj = JSON.parse( to );
			var result ='';
			obj.forEach(function(item) {
				result+=  '<a href="' + item.link + '" class="social-icon"><i class="fa ' + item.icon_value + '"></i></a>';
			});
			$( '.footer-social-links' ).html( result );
		} );
	} );

	/*********************************/
	/******  About us page  **********/
	/*********************************/
	wp.customize( 'tradiestandard_our_team_title', function( value ) {
		value.bind( function( to ) {
			$( '.meet-out-team-title' ).text( to );
		} );
	} );
	wp.customize( 'tradiestandard_our_team_subtitle', function( value ) {
		value.bind( function( to ) {
			$( '.meet-out-team-subtitle' ).text( to );
		} );
	} );
	wp.customize( 'tradiestandard_about_page_video_title', function( value ) {
		value.bind( function( to ) {
			$( '.video-title' ).text( to );
		} );
	} );
	wp.customize( 'tradiestandard_about_page_video_subtitle', function( value ) {
		value.bind( function( to ) {
			$( '.video-subtitle' ).text( to );
		} );
	} );
	wp.customize( 'tradiestandard_about_page_video_background', function( value ) {
		value.bind( function( to ) {
			$( '.about-page-video' ).css( 'background-image', 'url(' + to + ')' );
		} );
	} );
	wp.customize( 'tradiestandard_about_page_video_link', function( value ) {
		value.bind( function( to ) {
			if( to !== '' ) {
				$( '.video-box-icon' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '.video-box-icon' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}

		} );
	} );
	wp.customize( 'tradiestandard_our_advantages_title', function( value ) {
		value.bind( function( to ) {
			$( '.our_advantages' ).text( to );
		} );
	} );

	/* Team members (Repeater) */
	wp.customize( 'tradiestandard_team_members', function( value ) {
		value.bind( function( to ) {
			var obj = JSON.parse( to );
			var result ='';
			obj.forEach(function(item) {
				result += '<div class="col-sm-6 col-md-3 mb-sm-20 fadeInUp"><div class="team-item"><div class="team-image"><img src="' + item.image_url + '" alt="' + item.text + '"><div class="team-detail"><p class="font-serif">' + item.description + '</p></div><!-- .team-detail --></div><!-- .team-image --><div class="team-descr font-alt"><div class="team-name">' + item.text + '</div><div class="team-role">' + item.subtext + '</div></div><!-- .team-descr --></div><!-- .team-item --></div>';
			});
			$( '.about-team-member .slides' ).html( result );
		} );
	} );

	/* Advantages (Repeater) */
	wp.customize( 'tradiestandard_advantages', function( value ) {
		value.bind( function( to ) {
			var obj = JSON.parse( to );
			var result ='';
			obj.forEach(function(item) {
				result += '<div class="col-sm-6 col-md-3 col-lg-3"><div class="features-item"><div class="features-icon"><span class="' + item.icon_value + '"></span></div><h3 class="features-title font-alt">' + item.text + '</h3>' + item.subtext + '</div></div>';
			});
			$( '.module-advantages .multi-columns-row' ).html( result );
		} );
	} );
 

	/*********************************/
	/**********  404 page  ***********/
	/*********************************/
	wp.customize( 'tradiestandard_404_background', function( value ) {
		value.bind( function( to ) {
			$( '.error-page-background' ).css( 'background-image', 'url(' + to + ')' );
		} );
	} );
	wp.customize( 'tradiestandard_404_title', function( value ) {
		value.bind( function( to ) {
			$( '.error-page-title' ).html( to );
		} );
	} );
	wp.customize( 'tradiestandard_404_text', function( value ) {
		value.bind( function( to ) {
			$( '.error-page-text' ).html( to );
		} );
	} );
	wp.customize( 'tradiestandard_404_link', function( value ) {
		value.bind( function( to ) {
			$( '.error-page-button-text a' ).attr( 'href', to );
		} );
	} );
	wp.customize( 'tradiestandard_404_label', function( value ) {
		value.bind( function( to ) {
			$( '.error-page-button-text a' ).text( to );
		} );
	} );

} )( jQuery );

=======
/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {

	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).text( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description a' ).text( to );
		} );
	} );

	/****************************************/
	/********** Big title section ***********/
	/****************************************/
	wp.customize( 'tradiestandard_big_title_hide', function( value ) {
		value.bind( function( to ) {
			if( to !== true ) {
				$( '.home-section' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			} else {
				$( '.home-section' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}

		} );
	} );

	/******************************/
	/**********  Colors ***********/
	/******************************/

	wp.customize( 'header_textcolor', function( value ) {
		value.bind( function( to ) {
			$( '.tradiestandard_header_title h1 a' ).css( {
				'color': to
			} );
			$( '.tradiestandard_header_title h2 a' ).css( {
				'color': to
			} );
		} );
	} );
	wp.customize( 'background_color', function( value ) {
		value.bind( function( to ) {
			$( 'body' ).css( {
				'background': to
			} );
			if( $( '.front-page-main' ).length > 0 ) { 
				$( '.front-page-main' ).css( {
					'background': to
				} );
			}
		} );
	} );

    /******************************/
    /**********  Header ***********/
    /******************************/
    wp.customize( 'tradiestandard_logo', function( value ) {
        value.bind( function( to ) {

            if( to !== '' ) {
                $( '.tradiestandard_header_title .logo-image' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
                $( '.tradiestandard_header_title h1' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
                $( '.tradiestandard_header_title h2' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
            }
            else {
                $( '.tradiestandard_header_title .logo-image' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
                $( '.tradiestandard_header_title h1' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
                $( '.tradiestandard_header_title h2' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
            }
            $( '.tradiestandard_header_title img' ).attr( 'src', to );

        } );
    } );

    wp.customize( 'tradiestandard_blog_header_title', function( value ) {
        value.bind( function( to ) {
            if( to !== '' ) {
                $( '.tradiestandard-blog-header-title' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
            }
            else {
                $( '.tradiestandard-blog-header-title' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
            }
            $( '.tradiestandard-blog-header-title' ).html( to );
        } );
    } );

    wp.customize( 'tradiestandard_blog_header_subtitle', function( value ) {
        value.bind( function( to ) {
            if( to !== '' ) {
                $( '.tradiestandard-blog-header-subtitle' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
            }
            else {
                $( '.tradiestandard-blog-header-subtitle' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
            }
            $( '.tradiestandard-blog-header-subtitle' ).html( to );
        } );
    } );
	
	/*******************************/
	/******    Slider section ******/
	/*******************************/
	wp.customize( 'tradiestandard_slider_hide', function( value ) {
		value.bind( function( to ) {
			if( to !== '1' ) {
				$( 'section.home-section' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
				$( '.navbar-custom' ).removeClass( 'navbar-color-customizer' );
				$('.main').css('margin-top', 0 );
			}
			else {
				$( 'section.home-section' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
				$( '.navbar-custom' ).addClass( 'navbar-color-customizer' );
				$('.main').css('margin-top', $('.navbar-custom').outerHeight() );
			}
		} );
	} );

	/********************************/
    /*********	Banners section *****/
	/********************************/
	wp.customize( 'tradiestandard_banners_hide', function( value ) {
		value.bind( function( to ) {
			if( to !== true ) {
				$( '.home-banners' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '.home-banners' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}
		} );
	} );

	wp.customize( 'tradiestandard_banners_title', function( value ) {

		value.bind( function( to ) {
			if( to !== '' ) {
				$( '.product-banners-title' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '.product-banners-title' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			$( '.product-banners-title' ).text( to );
		} );

	} );

	// Add new banner (Repeater)
	wp.customize( 'tradiestandard_banners', function( value ) {
		value.bind( function( to ) {
			var obj = JSON.parse( to );
			var result ='';
			obj.forEach(function(item) {
				result += '<div class="col-sm-4"><div class="content-box mt-0 mb-0"><div class="content-box-image"><a href="' + item.link + '"><img src="' + item.image_url + '"></a></div></div></div>';
			});
			$( '.tradiestandard_bannerss_section' ).html( result );
		} );
	} );



	/*********************************/
    /*******  Products section *******/
	/********************************/
	wp.customize( 'tradiestandard_products_hide', function( value ) {
		value.bind( function( to ) {
			if( to !== true ) {
				$( '#latest' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '#latest' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}
		} );
	} );
	wp.customize( 'tradiestandard_products_title', function( value ) {
		value.bind( function( to ) {
			$( '.product-hide-title' ).html( to );
		} );
	} );

	/****************************************/
	/*********** Video section **************/
	/****************************************/
	wp.customize( 'tradiestandard_video_hide', function( value ) {
		value.bind( function( to ) {
			if( to !== true ) {
				$( '.module-video' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '.module-video' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}
		} );
	} );
	wp.customize( 'tradiestandard_video_title', function( value ) {
		value.bind( function( to ) {
			var $title = $( '.video-title' );
            if  ( to !== '' ) {
                $title.removeClass( 'tradiestandard_hidden_if_not_customizer' );
                $title.html( to );
            } else {
                $title.addClass('tradiestandard_hidden_if_not_customizer');
			}
		} );
	} );

    wp.customize('tradiestandard_video_section_padding', function (value) {
        var $selector = $('.module.module-video');
        value.bind(function (to) {
            $( window ).resize();
            $selector.css('padding', to+'px 0px');
        });
    });

	/****************************************/
    /*******  Products slider section *******/
	/****************************************/
	wp.customize( 'tradiestandard_products_slider_hide', function( value ) {
		value.bind( function( to ) {
			if( to !== true ) {
				$( '.home-product-slider' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '.home-product-slider' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}
		} );
	} );
	wp.customize( 'tradiestandard_products_slider_single_hide', function( value ) {
		value.bind( function( to ) {
			if( to !== true ) {
				$( '.module-small-bottom' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '.module-small-bottom' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}
		} );
	} );
	wp.customize( 'tradiestandard_products_slider_title', function( value ) {
		value.bind( function( to ) {
			$( '.home-prod-title' ).text( to );
		} );
	} );
	wp.customize( 'tradiestandard_products_slider_subtitle', function( value ) {
		value.bind( function( to ) {
			$( '.home-prod-subtitle' ).text( to );
		} );
	} );
	
	/****************************************/
	/*********  Services section ************/
	/****************************************/

	wp.customize( 'tradiestandard_services_title', function( value ) {
		value.bind( function( to ) {
			$( '.services h2' ).text( to );
		} );
	} );
	wp.customize( 'tradiestandard_services_subtitle', function( value ) {
		value.bind( function( to ) {
			$( '.services .module-subtitle' ).text( to );
		} );
	} );
	
	/* Our services (Repeater) */
	wp.customize( 'tradiestandard_service_box', function( value ) {
		value.bind( function( to ) {
			var obj = JSON.parse( to );
			var result ='';
			obj.forEach(function(item) {
				result += '<div class="col-xs-12 col-sm-4 sip-service-box"><div class="sip-single-service"><a href="'+item.link+'" class="sip-single-service-a"><span class="'+item.icon_value+' sip-single-icon"></span><div class="sip-single-service-text"><h3 class="">'+item.text+'</h3><p class="">'+item.subtext+'</p></div></a></div></div>';
			});
			$( '#our_services_wrap' ).html( result );
		} );
	} );

	/****************************************/
	/*********  Ribbon section **************/
	/****************************************/
	wp.customize( 'tradiestandard_ribbon_text', function( value ) {
		value.bind( function( to ) {
			if( to !== '1' ) {
				$( '.ribbon' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '.ribbon' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}
		} );
	} );
	wp.customize( 'tradiestandard_ribbon_text', function( value ) {
		value.bind( function( to ) {
			$( '.ribbon h2' ).text( to );
		} );
	} );

	wp.customize( 'tradiestandard_ribbon_button_text', function( value ) {
		value.bind( function( to ) {
			$( '.ribbon .btn-ribbon' ).text( to );
		} );
	} );

	wp.customize( 'tradiestandard_ribbon_background', function( value ) {
		value.bind( function( to ) {
			$( '#ribbon' ).css( 'background-image', 'url(' + to + ')' );
		} );
	} );

	/*****************************************/
	/*********  Categories section ***********/
	/*****************************************/

	wp.customize( 'tradiestandard_fp_categories_title', function( value ) {
		value.bind( function( to ) {
			$( '.categories#categories h2' ).text( to );
		} );
	} );
	wp.customize( 'tradiestandard_fp_categories_subtitle', function( value ) {
		value.bind( function( to ) {
			$( '.categories#categories .module-subtitle' ).text( to );
		} );
	} );

	/*******************************/
    /***********  Footer ***********/
	/*******************************/
	wp.customize( 'tradiestandard_copyright', function( value ) {
		value.bind( function( to ) {
			$( '.copyright' ).text( to );
		} );
	} );


	/*******************************************/
	/******    Hide site info from footer ******/
	/*******************************************/
	wp.customize( 'tradiestandard_site_info_hide', function( value ) {
		value.bind( function( to ) {
			if( to !== true ) {
				$( '.tradiestandard-poweredby-box' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '.tradiestandard-poweredby-box' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}
		} );
	} );

	// socials (Repeater)
	wp.customize( 'tradiestandard_socials', function( value ) {
		value.bind( function( to ) {
			var obj = JSON.parse( to );
			var result ='';
			obj.forEach(function(item) {
				result+=  '<a href="' + item.link + '" class="social-icon"><i class="fa ' + item.icon_value + '"></i></a>';
			});
			$( '.footer-social-links' ).html( result );
		} );
	} );

	/*********************************/
	/******  About us page  **********/
	/*********************************/
	wp.customize( 'tradiestandard_our_team_title', function( value ) {
		value.bind( function( to ) {
			$( '.meet-out-team-title' ).text( to );
		} );
	} );
	wp.customize( 'tradiestandard_our_team_subtitle', function( value ) {
		value.bind( function( to ) {
			$( '.meet-out-team-subtitle' ).text( to );
		} );
	} );
	wp.customize( 'tradiestandard_about_page_video_title', function( value ) {
		value.bind( function( to ) {
			$( '.video-title' ).text( to );
		} );
	} );
	wp.customize( 'tradiestandard_about_page_video_subtitle', function( value ) {
		value.bind( function( to ) {
			$( '.video-subtitle' ).text( to );
		} );
	} );
	wp.customize( 'tradiestandard_about_page_video_background', function( value ) {
		value.bind( function( to ) {
			$( '.about-page-video' ).css( 'background-image', 'url(' + to + ')' );
		} );
	} );
	wp.customize( 'tradiestandard_about_page_video_link', function( value ) {
		value.bind( function( to ) {
			if( to !== '' ) {
				$( '.video-box-icon' ).removeClass( 'tradiestandard_hidden_if_not_customizer' );
			}
			else {
				$( '.video-box-icon' ).addClass( 'tradiestandard_hidden_if_not_customizer' );
			}

		} );
	} );
	wp.customize( 'tradiestandard_our_advantages_title', function( value ) {
		value.bind( function( to ) {
			$( '.our_advantages' ).text( to );
		} );
	} );

	/* Team members (Repeater) */
	wp.customize( 'tradiestandard_team_members', function( value ) {
		value.bind( function( to ) {
			var obj = JSON.parse( to );
			var result ='';
			obj.forEach(function(item) {
				result += '<div class="col-sm-6 col-md-3 mb-sm-20 fadeInUp"><div class="team-item"><div class="team-image"><img src="' + item.image_url + '" alt="' + item.text + '"><div class="team-detail"><p class="font-serif">' + item.description + '</p></div><!-- .team-detail --></div><!-- .team-image --><div class="team-descr font-alt"><div class="team-name">' + item.text + '</div><div class="team-role">' + item.subtext + '</div></div><!-- .team-descr --></div><!-- .team-item --></div>';
			});
			$( '.about-team-member .slides' ).html( result );
		} );
	} );

	/* Advantages (Repeater) */
	wp.customize( 'tradiestandard_advantages', function( value ) {
		value.bind( function( to ) {
			var obj = JSON.parse( to );
			var result ='';
			obj.forEach(function(item) {
				result += '<div class="col-sm-6 col-md-3 col-lg-3"><div class="features-item"><div class="features-icon"><span class="' + item.icon_value + '"></span></div><h3 class="features-title font-alt">' + item.text + '</h3>' + item.subtext + '</div></div>';
			});
			$( '.module-advantages .multi-columns-row' ).html( result );
		} );
	} );
 

	/*********************************/
	/**********  404 page  ***********/
	/*********************************/
	wp.customize( 'tradiestandard_404_background', function( value ) {
		value.bind( function( to ) {
			$( '.error-page-background' ).css( 'background-image', 'url(' + to + ')' );
		} );
	} );
	wp.customize( 'tradiestandard_404_title', function( value ) {
		value.bind( function( to ) {
			$( '.error-page-title' ).html( to );
		} );
	} );
	wp.customize( 'tradiestandard_404_text', function( value ) {
		value.bind( function( to ) {
			$( '.error-page-text' ).html( to );
		} );
	} );
	wp.customize( 'tradiestandard_404_link', function( value ) {
		value.bind( function( to ) {
			$( '.error-page-button-text a' ).attr( 'href', to );
		} );
	} );
	wp.customize( 'tradiestandard_404_label', function( value ) {
		value.bind( function( to ) {
			$( '.error-page-button-text a' ).text( to );
		} );
	} );

} )( jQuery );

>>>>>>> 2f60927d06dd186fefafb15442a32abd421ec7aa
