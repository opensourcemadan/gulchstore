<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package tradiestandard
 */

if ( ! is_active_sidebar( 'tradiestandard-sidebar-shop-archive' ) ) {
	return;
}
?>

<div id="secondary" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'tradiestandard-sidebar-shop-archive' ); ?>
</div><!-- #secondary -->
