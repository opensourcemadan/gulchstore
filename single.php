<?php
/**
 * The template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

get_header(); ?>

<!-- Wrapper start -->
	<div class="main">

		<!-- Post single start -->
		<section class="page-module-content module">
			<div class="container">

				<div class="row">

					<!-- Content column start -->
					<div class="col-sm-8">

						<?php while ( have_posts() ) : the_post(); ?>

							<?php

							do_action( 'tradiestandard_single_post_before' );

							get_template_part( 'content', 'single' );

							/**
							 * After single post hook.
							 *
							 * @hooked tradiestandard_post_nav - 10
							 */
							do_action( 'tradiestandard_single_post_after' );
							?>

						<?php endwhile; // end of the loop. ?>

					</div>
					<!-- Content column end -->

					<!-- Sidebar column start -->
					<div class="col-xs-12 col-sm-4 col-md-3 col-md-offset-1 sidebar">

						<?php do_action( 'tradiestandard_sidebar' ); ?>

					</div>
					<!-- Sidebar column end -->

				</div><!-- .row -->

			</div>
		</section>
		<!-- Post single end -->
		

<?php get_footer(); ?>
