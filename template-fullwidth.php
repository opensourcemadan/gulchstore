<?php
/**
 * The template for displaying full width pages.
 *
 * Template Name: Full width
 *
 * @package WordPress
 * @subpackage tradiestandard
 */

get_header(); ?>

<!-- Wrapper start -->
	<div class="main">
	
		<!-- Header section start -->
		<?php
		$tradiestandard_header_image = get_header_image();
		if ( ! empty( $tradiestandard_header_image ) ) :
			echo '<section class="page-header-module module bg-dark" data-background="' . esc_url( $tradiestandard_header_image ) . '">';
		else :
			echo '<section class="page-header-module module bg-dark">';
		endif;
		?>
			<div class="container">

				<div class="row">

					<div class="col-sm-10 col-sm-offset-1">
						<h1 class="module-title font-alt"><?php the_title(); ?></h1>
					</div>

				</div><!-- .row -->

			</div>
		</section>
		<!-- Header section end -->
		
		

		<!-- Pricing start -->
		<section class="module">
			<div class="container">
			
				<div class="row">

					<!-- Content column start -->
					<div class="col-sm-12">
			
					<?php
					/**
					 * Top of the content hook.
					 *
					 * @hooked woocommerce_breadcrumb - 10
					 */
					do_action( 'tradiestandard_content_top' ); ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php
						do_action( 'tradiestandard_page_before' );
						?>

						<?php get_template_part( 'content', 'page' ); ?>

						<?php
						/**
						 * Bottom of content hook.
						 *
						 * @hooked tradiestandard_display_comments - 10
						 */
						do_action( 'tradiestandard_page_after' );
						?>

					<?php endwhile; // end of the loop. ?>
					
					</div>
					
				</div> <!-- .row -->	

			</div>
		</section>
		<!-- Pricing end -->


<?php get_footer(); ?>
